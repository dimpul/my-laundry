<style type="text/css">
    .menu{
        cursor: pointer;
    }
    .menu.selected{
        background-color: #c9fcff;
    }

    .submenu{
        cursor: pointer;
    }
    .submenu.selected{
        background-color: #fcf99f;
    }
</style>
<!-- Custom Header for this page -->

  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Data table</h4>
      <div class="row" style="padding-bottom: 10px;">
        <div class="col-md-7 cent-left"><h3 class="">Data Role</h3></div>
          <div class="col-md-5 cent-right" id="btn_config">
              <button class="btn btn-success btn-sm" id="tambah"><i class="fa fa-plus"></i></button>
              <button class="btn btn-warning btn-sm cent-hidden" id="update"><i class="fa fa-pencil"></i></button>
              <button class="btn btn-danger btn-sm cent-hidden" id="delete"><i class="fa fa-close"></i></button>
            </div>
        </div>
      <div class="row">
        <div class="col-12">
          <div class="table-responsive">
            <table class="table table-hover datatable">
              <thead>
                <tr>
                    <th>Nama Role</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  foreach ($list as $row) { ?>
                    <tr id="tr-<?= $row->id_role; ?>" onclick="click_menu('<?= $row->id_role; ?>','role')" class="menu">

                        <td> <?= $row->nama_role; ?> </td>
                    </tr>

                    
                 <?php  } ?>
                

              </tbody>
            </table>
            
            
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- Custom Footer for this page -->
<!-- MODAL -->
<div class="modal fade" id="modal_tambah" tabindex="-1" role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalLabel">Title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> 
      </div>
      <form id="form_menu" data-toggle="validator" role="form">
      <div class="modal-body" id="ModalBody"></div>
      </form>
      <div class="modal-footer">
        <button type="button" id="submit" class="btn btn-success">Submit</button>
        <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div> 
</div>
<!-- Modal Ends -->

<!-- Custom Footer for this page -->
<script type="text/javascript">
    var id = null;
    var action = null;
    var parent = null;
    function click_menu(id_role,type){
        $(".menu").removeClass("selected");
        $("#tr-"+id_role).addClass("selected");
        id = id_role;
        
        $("#btn_config").addClass("show");
        $("#tambah2").removeClass("cent-hidden");
        $("#update2").addClass("cent-hidden");
        $("#delete2").addClass("cent-hidden");
        $("#access2").addClass("cent-hidden");
        $("#update").removeClass("cent-hidden");
        $("#access").removeClass("cent-hidden");
        $("#delete").removeClass("cent-hidden");
      
    }

    function set_label(id,text){
        $("#"+id).empty();
        $("#"+id).append(text);

    }

    function type_text(val,id){
        var value = '';
        if(id=='uri'){
            value = '<?= base_url(); ?>'+val;
            $("#"+id).empty();
            set_label(id,value);
        }else if(id=='fa_icon'){
            value = 'Fa Icon => <i class="fa fa-'+val+'"></i>';
            $("#"+id).empty();
            set_label(id,value);
        }
    }

    function generate_form(data){
        //variable data dengan kolom label, name, dan value
        var form = "";
        $.each(data , function(index, val) { 
           form += '<div class="form-group" '+val.else+'>';
           form += '<label>'+val.label+'</label>';
           form += '<input onkeyup="type_text(this.value,'+"'"+val.name+"'"+');" name="'+val.name+'" class="form-control" placeholder="Enter '+val.label+'" value="'+val.value+'"  required>';
           form += '<small id="'+val.name+'" class="form-text text-muted"></small>';
           form += '</div>';
        });
        return form;
    }
    $( document ).ready(function() {
        $("#tambah").click(function(){
           action = '<?= base_url("role/tambah_conf"); ?>';
           var title = '<i class="fa fa-plus"></i> Tambah Role';
           set_label("ModalLabel",title);
           var data = [
             {"label":"Nama Role","name":"nama_role","value":"", "else":""},  
           ];
           var form = generate_form(data);
           set_label("ModalBody",form);
           $("#modal_tambah").modal('show');
        }); 
        $("#update").click(function(){
           action = '<?= base_url("role/update_conf"); ?>';
           set_label("ModalBody","<center><i class='fa fa-2x fa-spin fa-spinner'> </i></center>");

           //get data with ajax
           var data = get_ajax("id="+id, "role/get_update");
           var datax = [];
           setTimeout(function() {
                var tmp = JSON.parse(data.responseText);
                var tmp2 = JSON.parse(tmp.message);
                console.log(tmp2);
                var datay = [
                {"label":"Id ","name":"id_role","value":tmp2.id_role},
               {"label":"Nama Role ","name":"nama_role","value":tmp2.nama_role}
               
               ];
               var form = generate_form(datay);
               set_label("ModalBody",form);
            }, 1000);
           
           var title = '<i class="fa fa-pencil"></i> Update Role';
           set_label("ModalLabel",title);
           $("#modal_tambah").modal('show');
        });

        $("#delete").click(function(){
           action = '<?= base_url("Role/delete_conf"); ?>';
           pesan_confirm("Apakah anda yakin?", "Menghapus Role?", "Ya, Hapus").then((result) => {
            if(result===true){
                simple_ajax('id='+id,action);
            }
        });
        });

      //submit
        $("#submit").click(function(){
            $("#form_menu").submit();
        });

        $('#form_menu').submit(function(event) { 
            event.preventDefault(); 
            var values = $(this).serialize();
            simple_ajax(values,action);
            return false; //stop
        });     
    });
</script>