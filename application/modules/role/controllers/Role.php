<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller {

    public function index()
    {
     #Bagian Inisialisasi
    	$datamenu = array();
      #Bagian Proses
    	$datamenu = h_crud_get_data('role_tb');
      #Bagian Return
    	$data['list'] = $datamenu;
        load_view('list-role',$data,'Management Role');
    }
    public function get_update()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $id             = $this->input->post('id');

      #Bagian Proses
        $where          = array('id_role'=>$id);
        $data_menu      = h_crud_get_1_data('role_tb',$where);
        $str_data_menu  = json_encode($data_menu);

      #Bagian Return
        $return = array('stt'=>1,'message'=>$str_data_menu);
        echo json_encode($return);
    }

   
    public function tambah_conf()
    {
     #Bagian Inisialisasi
      $nama = $this->input->post('nama_role');
      $data = array('nama_role'=>$nama);
     #Bagian Proses
      $execute = h_crud_tambah('role_tb', $data);
       
     #Bagian Return
      if($execute){
          $this->db->trans_complete();
          return_ajax(1,"Berhasil Menambah Role");
        }else{
          return_ajax(0,"Gagal Menambah Role");
        } 
    }
    public function delete_conf()
    {
     #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();
        $id             = $this->input->post('id');
      #Bagian Proses
        $execute = h_crud_delete('role_tb','id_role',$id);
      #Bagian Return
        if(!$execute){
          $return = array('stt'=>0,'message'=>'Gagal Menghapus role');
        }else{
          $this->db->trans_complete();
          $return = array('stt'=>1,'message'=>'Berhasil Menghapus Role');
        }
        $this->db->trans_complete();
        echo json_encode($return);
    
    }
    public function update_conf()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();
        $id        = $this->input->post('id_role');
        $nama_role      = $this->input->post('nama_role');
       
        $data = array( 
          "nama_role"=>$nama_role
        );
      #Bagian Proses
        $execute = h_crud_update('role_tb',$data,'id_role',$id);
       
      #Bagian Return
        if($execute){
         
          $return = array('stt'=>1,'message'=>'Berhasil Update Role');
        }
        else{
          $return = array('stt'=>0,'message'=>'Gagal Update Role');
        }
       $this->db->trans_complete();
      echo json_encode($return);
    }
}