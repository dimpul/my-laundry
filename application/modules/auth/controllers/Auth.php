<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
    {
	    parent::__construct();
      $this->load->library('password');
    }

    public function index()
    {
       
      $sess = get_session('id_user');
      if(!empty($sess)){
        redirect(base_url('home'));
      }
       load_view_login('login');
    }

    public function conf()
    {  
      #Bagian Inisialisasi
        check_ajax_request();
        $username = post('username');
        $password = post('password');

      #Bagian Proses
    	  //cek input
        if(empty($username) || empty($password)){
          $return = array('stt'=>0,'message'=>'Mohon Masukkan Username/Password');
          echo json_encode($return);
          return 0;
        }

        //cek username/email
        if (preg_match('/\b@\b/', $username)) {
            $where = array("email"=>$username,"status_akun"=>1);
        }else{
            $where = array("username"=>$username,"status_akun"=>1);
        }
        $data = h_crud_get_1_data("user_tb",$where);
        if(empty($data)){
          $return = array('stt'=>0,'message'=>'User Tidak Ditemukan');
          echo json_encode($return);
          return 0;
        }

        //cek password
        $cek = $this->password->custom_verify($password,$data->password); 
        if($cek){
          $sess = array(
            'id_user'  => $data->id_user,
            'nama'  => $data->nama_lengkap,
            'role'     => $data->role
          );

          $this->session->set_userdata($sess);
          $return = array('stt'=>1,'message'=>'Login Berhasil');
        }else{
          $return = array('stt'=>0,'message'=>'Maaf, Data tidak sesuai');
        }
        echo json_encode($return);
        return 0;

      #Bagian Return
    	//null
    }

    public function logout()
    {  
      #Bagian Inisialisasi
      #Bagian Proses
      $this->session->sess_destroy();
      #Bagian Return
      load_view_login('login');
    }

}