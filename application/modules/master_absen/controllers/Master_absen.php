<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_absen extends CI_Controller {

	public function __construct()
    {
	    parent::__construct();
      $this->load->library('password');
    }


    public function index()
    {
       #Bagian Inisialisasi
        $data_master = array();
        
      #Bagian Proses
        $data_master = h_crud_get_data('master_absen_tb');
        
      #Bagian Return
        $data['list']   = $data_master;
        load_view('list',$data,'List Master Absen');
    }

    public function get_form_master()
    {  
      #Bagian Inisialisasi
         $type = post('type');
         $id   = post('id');

      #Bagian Proses
         if($type=="update"){
            $data_checking_empty = array($id);
            checking_empty($data_checking_empty);
            $where      = array("id_master_absen"=>$id);
            $data_master  = h_crud_get_1_data("master_absen_tb",$where);

            if(empty($data_master)){
               echo "<center>Data Tidak Ditemukan</center>";
               return 0;
            }
         }else{
            $data_master  = null;
         }


      #Bagian Return
         $data['master'] = $data_master;
         $data['type'] = $type;
         load_empty_view('form_master',$data);
    }

    public function get_form_password()
    {  
      #Bagian Inisialisasi
      #Bagian Proses
      #Bagian Return
         load_empty_view('form_password');
    }

     public function tambah_conf()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();

        $nama          = post('nama_master_absen');
        $jam_masuk     = post('jam_masuk');
        $jam_pulang    = post('jam_pulang');

        $data_checking_empty = array($nama,$jam_masuk,$jam_pulang);
        checking_empty($data_checking_empty);

      #Bagian Proses
        //eksekusi guys
        $data = array(
          "nama_master_absen"=>$nama,
          "jam_masuk"=>$jam_masuk,
          "jam_pulang"=>$jam_pulang
        );
        $execute = h_crud_tambah('master_absen_tb', $data);

      #Bagian Return
        if($execute){
          $this->db->trans_complete();
          return_ajax(1,"Berhasil Menambah Master Absen");
        }else{
          return_ajax(0,"Gagal Menambah Master Absen");
        }
    }

    

    public function delete_conf()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();
        $id             = $this->input->post('id');
        $data_checking_empty = array($id);
        checking_empty($data_checking_empty);

      #Bagian Proses
        if($id==1){
          return_ajax(0,"Maaf, Master Absen OFF tidak dapat dihapus");
        }

        $execute = h_crud_delete('master_absen_tb','id_master_absen',$id);
        
      #Bagian Return
        if(!$execute){
          return_ajax(0,"Gagal Menghapus Master Absen");
        }else{
          $this->db->trans_complete();
          return_ajax(1,"Berhasil Menghapus Master Absen");
        }
    }

    public function update_conf(){
       #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();

        $nama          = post('nama_master_absen');
        $jam_masuk     = post('jam_masuk');
        $jam_pulang    = post('jam_pulang');
        $id            = post('id');
        $data_checking_empty = array($nama,$jam_masuk,$jam_pulang, $id);
        checking_empty($data_checking_empty);

      #Bagian Proses 
        //eksekusi guys
        $data = array(
          "nama_master_absen"=>$nama,
          "jam_masuk"=>$jam_masuk,
          "jam_pulang"=>$jam_pulang
        );
        $execute = h_crud_update('master_absen_tb',$data,'id_master_absen',$id);
        
      #Bagian Return
        if($execute){
          $this->db->trans_complete();
          return_ajax(1,"Berhasil Mengubah Master Absen");
        }else{
          return_ajax(0,"Gagal Mengubah Master Absen");
        }
        echo json_encode($return);
    }
}