<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {

	public function __construct()
    {
	    parent::__construct();
      check_session();
      $this->load->model('m_karyawan');
      $this->load->library('password');
    }

    public function index()
    {
       #Bagian Inisialisasi
        $data_user = array();
        
      #Bagian Proses
        $data_user = $this->m_karyawan->get_karyawan();
        
      #Bagian Return
        $data['list']   = $data_user;
        load_view('list',$data,'List Karyawan');
    }

    public function form($type,$id=""){
      #Bagian Inisialisasi
      #Bagian Proses
         if($type=="update"){
            $where      = array("id_user"=>$id);
            $data_user  = h_crud_get_1_data("user_tb",$where);

            if(empty($data_user)){
               redirect('karyawan');
               return 0;
            }
         }else{
            $data_user  = null;
         }

         $data_role_user = $this->m_karyawan->get_role_user();

      #Bagian Return
         $data['user'] = $data_user;
         $data['type'] = $type;
         $data['role'] = $data_role_user;
         load_view('form_karyawan',$data);
    }

    public function get_form_user()
    {  
      #Bagian Inisialisasi
         $type = post('type');
         $id   = post('id');

      #Bagian Proses
         if($type=="update"){
            $where      = array("id_user"=>$id);
            $data_user  = h_crud_get_1_data("user_tb",$where);

            if(empty($data_user)){
               echo "<center>Data Tidak Ditemukan</center>";
               return 0;
            }
         }else{
            $data_user  = null;
         }

         $data_role_user = $this->m_karyawan->get_role_user();

      #Bagian Return
         $data['user'] = $data_user;
         $data['type'] = $type;
         $data['role'] = $data_role_user;
         load_empty_view('form_karyawan',$data);
    }

    public function get_form_password()
    {  
      #Bagian Inisialisasi
        check_ajax_request();
      #Bagian Proses
      #Bagian Return
         load_empty_view('form_password');
    }

    public function get_form_password_karyawan()
    {  
      #Bagian Inisialisasi
        check_ajax_request();
      #Bagian Proses
      #Bagian Return
         load_empty_view('form_password_karyawan');
    }


    public function get_ktp()
    {  
      #Bagian Inisialisasi
        check_ajax_request();
        $id = post('id');
      #Bagian Proses
        $ktp  = $this->m_karyawan->get_ktp($id); 
      #Bagian Return
        $data['ktp'] = $ktp;
        load_empty_view('form_ktp',$data);
    }

    public function upload_file_ktp($new_name){
      $config['upload_path']          = $_SERVER['DOCUMENT_ROOT'].'/assets/scan_ktp/';
      //$config['upload_path']          = $_SERVER['DOCUMENT_ROOT'].'/laundry/assets/scan_ktp';
      $config['allowed_types']        = 'jpeg|jpg|png';
      $config['max_size']             = 1024;
      $config['file_name'] = $new_name;
      $this->load->library('upload', $config);
      if ( ! $this->upload->do_upload('ktp')){
        return 0;
      }else{
        return 1;
      }
    }

     public function tambah_conf()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();

        $name      = post('name');
        $email     = post('email');
        $username  = post('username');
        $role      = 3;
        $no_hp     = post('no_hp');
        $alamat    = post('alamat');
        $jenis_kelamin = post('jeniskelamin');
        $date = strtotime(post('tgl_lahir'));
        $tgl_lahir = date('Y-m-d', $date); 

      #Bagian Proses
        //generate id finger 
        $id_finger = $this->m_karyawan->get_id_finger();
        //generate password default
        $password  =  $this->password->custom_hash('654321');
        //change nama file ktp
        $format = '.'.pathinfo($_FILES['ktp']['name'], PATHINFO_EXTENSION);
        $new_name = "ktp_".$name.$format;

        //tambah karyawan
        $data = array(
          "nama_lengkap"=>$name,
          "email"=>$email,
          "username"=>$username,
          "status_akun"=>1,
          "role"=>$role,
          "alamat"=>$alamat,
          "jenis_kelamin"=>$jenis_kelamin,
          "no_hp"=>$no_hp,
          "tgl_lahir"=>$tgl_lahir,
          "password"=>$password,
          "ktp"=>$new_name,
          "id_finger"=>$id_finger,
        );
        $execute = h_crud_tambah('user_tb', $data);
        if(!$execute){ //gagal tambah
            $return = array('stt'=>0,'message'=>'Gagal Tambah Data User');
            echo json_encode($return);
            return 0;
        }

        //upload file ktp
        $execute = $this->upload_file_ktp($new_name);
        if($execute){
          $this->db->trans_complete(); 
          $return = array('stt'=>1,'message'=>'Berhasil Menambahkan Karyawan');
          echo json_encode($return);
          return 0;
        }else{
          $return = array('stt'=>0,'message'=>'Gagal Upload file, cek tipe file atau ukuran file');
          echo json_encode($return);
          return 0;
        }
            
      #Bagian Return
        //null
    }

    public function cek_email()
    {  
      #Bagian Inisialisasi
         $email = post('email');

      #Bagian Proses
         $where      = array("email"=>$email);
         $data_email = h_crud_get_1_data("user_tb",$where);
         if(empty($data_email)){
            $return = '<small class="text-success"> E-Mail Dapat Digunakan </small>';
            $return .= '<script> $("#submit-btn").removeAttr("disabled");</script>';
         }else{
            $return = '<small class="text-danger"> E-Mail Tidak Dapat Digunakan </small>';
            $return .= '<script> $("#submit-btn").attr("disabled","disabled");</script>';
         }

      #Bagian Return
         echo $return;
    }

    public function cek_username()
    {  
      #Bagian Inisialisasi
         $username = post('username');

      #Bagian Proses
         $where      = array("username"=>$username);
         $data       = h_crud_get_1_data("user_tb",$where);
         if(empty($data)){
            $return = '<small class="text-success"> Username Dapat Digunakan </small>';
            $return .= '<script> $("#submit-btn").removeAttr("disabled");</script>';
         }else{
            $return = '<small class="text-danger"> Username Tidak Dapat Digunakan </small>';
            $return .= '<script> $("#submit-btn").attr("disabled","disabled");</script>';
         }

      #Bagian Return
         echo $return;
    }

    public function delete_conf()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();
        $id             = $this->input->post('id');
      #Bagian Proses
        $execute = h_crud_delete('user_tb','id_user',$id);
        if(!$execute){
          $return = array('stt'=>0,'message'=>'User memiliki data absen atau history keuangan');
        }else{
          $this->db->trans_complete();
          $return = array('stt'=>1,'message'=>'Berhasil Menghapus User');
        }
        
      #Bagian Return
        echo json_encode($return);
    }

    public function update_conf()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();

        $id        = post('id');
        $name      = post('name');
        $email     = post('email');
        $username  = post('username');
        $role      = 3;
        $no_hp     = post('no_hp');
        $alamat    = post('alamat');
        $jenis_kelamin = post('jeniskelamin');
        $date = strtotime(post('tgl_lahir'));
        $tgl_lahir = date('Y-m-d', $date); 

      #Bagian Proses
        //change nama file ktp
        if(!empty($_FILES['ktp']['name'])){
            $format = '.'.pathinfo($_FILES['ktp']['name'], PATHINFO_EXTENSION);
            $new_name = "ktp_".$name.$format;

            $data = array(
              "nama_lengkap"=>$name,
              "email"=>$email,
              "username"=>$username,
              "status_akun"=>1,
              "role"=>$role,
              "alamat"=>$alamat,
              "jenis_kelamin"=>$jenis_kelamin,
              "no_hp"=>$no_hp,
              "tgl_lahir"=>$tgl_lahir,
              "ktp"=>$new_name
            );

            //upload file ktp
            $execute = $this->upload_file_ktp($new_name);
            if(!$execute){
              $return = array('stt'=>0,'message'=>'Gagal Upload file, cek tipe file atau ukuran file');
              echo json_encode($return);
              return 0;
            }

        }else{
            $data = array(
              "nama_lengkap"=>$name,
              "email"=>$email,
              "username"=>$username,
              "status_akun"=>1,
              "role"=>$role,
              "alamat"=>$alamat,
              "jenis_kelamin"=>$jenis_kelamin,
              "no_hp"=>$no_hp,
              "tgl_lahir"=>$tgl_lahir
            );
        }
            
      #Bagian Return
        $execute = h_crud_update('user_tb', $data,"id_user",$id);
        if(!$execute){ 
            $return = array('stt'=>0,'message'=>'Gagal Update Data User');
            echo json_encode($return);
            return 0;
        }else{
            $this->db->trans_complete(); 
            $return = array('stt'=>1,'message'=>'Berhasil Update Karyawan');
            echo json_encode($return);
            return 0;
        }
    }

    public function password_conf(){
       #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();
        $password  = post('password');
        $id_user   = post('id');

      #Bagian Proses 
        $pwd = $this->password->custom_hash($password);
        $data = array(
          "password"=>$pwd
        );
        $execute = h_crud_update('user_tb',$data,'id_user',$id_user);

      #Bagian Return
        if($execute){
          $return = array('stt'=>1,'message'=>'Berhasil Update Password');
          $this->db->trans_complete();
        }else{
          $return = array('stt'=>0,'message'=>'Gagal Update Password');
        }
        echo json_encode($return);
    }

    public function profile(){
       #Bagian Inisialisasi
        

      #Bagian Proses 
        $pwd = $this->password->custom_hash($password);
        $data = array(
          "password"=>$pwd
        );
        $execute = h_crud_update('user_tb',$data,'id_user',$id_user);

      #Bagian Return
        if($execute){
          $return = array('stt'=>1,'message'=>'Berhasil Update Password');
          $this->db->trans_complete();
        }else{
          $return = array('stt'=>0,'message'=>'Gagal Update Password');
        }
        echo json_encode($return);
    }

}