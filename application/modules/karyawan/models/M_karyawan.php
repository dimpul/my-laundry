<?php
class M_karyawan extends CI_Model {
   public function __construct()
   {
        parent::__construct();
   }

   public function get_karyawan()
   {
        #get role without family, admin, and super user
        $role = array(3);
        $this->db->select('*');
        $this->db->from('user_tb');
        $this->db->where_in('role',$role);
        return $this->db->get()->result();
   }

   public function get_ktp($id_user)
   {
        #get ktp
        $this->db->where('id_user',$id_user);
        $this->db->select('ktp');
        $this->db->from('user_tb');
        $data = $this->db->get()->row();
        if(empty($data)){
          return 0;
        }else{
          return $data->ktp;
        }
   }

   public function get_role_user()
   {
        #get role without karyawan and super user
        $role = array(1,2);
        $this->db->select('*');
        $this->db->from('role_tb');
        $this->db->where_not_in('id_role',$role);
        return $this->db->get()->result();
   }

   public function get_id_finger()
   {
        #get role without karyawan and super user
        $this->db->select('max(id_finger) as max');
        $this->db->from('user_tb');
        $max = $this->db->get()->row()->max;
        return $max+1;
   }

   
}

?>