<?php
	#inisialisasi
	$id_user	= @$user->id_user;
	$nama 		= @$user->nama_lengkap;
	$email 		= @$user->email;
	$username = @$user->username;
  $status   = @$user->status_akun;
  $no_hp    = @$user->no_hp;
  $alamat   = @$user->alamat;
  $rolex    = @$user->role;
  $jeniskelamin    = @$user->jenis_kelamin;
  $date = strtotime(@$user->tgl_lahir);
  $tgl_lahir = date('d-m-Y', $date); 
?>

<form id="global-form">


    <div class="card">

      <div class="row">
        <div class="col-md-12">

        <div class="card-body">

            <div class="cent-left"><h3 class="">Data Karyawan</h3></div><br>
            <div id="div-alert" class="alert alert-warning" style="display: none;"> </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Nama Lengkap</label>
                  <input class="form-control" name="name" minlength="2" type="text" value="<?= $nama ?>" required>
                </div>

                <div class="form-group">
                  <label>Jenis Kelamin</label>
                  <div class="form-check" >
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="L" name="jeniskelamin" required="" <?= check_checked($jeniskelamin,'L'); ?>>
                      Laki-Laki
                    </label>
                  </div>
                  <div class="form-check" style="display: inline-block;">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" value="P" name="jeniskelamin" required="" <?= check_checked($jeniskelamin,'P'); ?>>
                      Perempuan
                    </label>
                  </div>
                </div>
                
                <div class="form-group ">
                  <label>Tanggal Lahir</label>
                  <div class="input-group date">
                    <input type="text" name="tgl_lahir" class="form-control" value="<?= $tgl_lahir; ?>">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
                </div>

                <div class="form-group ">
                  <label>Alamat</label>
                  <textarea style="min-height:120px; " class="form-control" name="alamat" required=""><?= $alamat; ?></textarea>
                </div>

              </div>
              <div class="col-md-6">
                <div class="form-group"> 
                  <label>No HP (Whatsapp)</label>
                  <input class="form-control" name="no_hp" type="number" value="<?= $no_hp ?>" required>
                </div>
                <div class="form-group"> 
                  <label>E-Mail</label>
                  <input class="form-control" name="email" type="email" id="email" value="<?= $email ?>">
                  <div id="imel"></div>
                </div>
                <div class="form-group">
                  <label>Username</label>
                  <input class="form-control" id="username" name="username" minlength="4" type="text" value="<?= $username ?>" required>
                  <div id="usrnm"></div>
                </div>

                <div class="form-group">
                  <label>Scan KTP</label>
                  <input class="form-control" name="ktp" type="file" accept="image/x-png,image/jpg,image/jpeg">
                   <small class="text-success"> Max ukuran 1 MB dan tipe .jpg/.jpeg/.png </small>  
                </div>
                <?php
                if($type=="update"){
                  $url = base_url('karyawan/update_conf'); ?>
                  <div class="form-group">
                      <label>Status</label><br>
                      <select class="form-control select2" name="status" style=""  required="">
                        <option value="1" <?= check_selected($status, 1); ?>> Aktif </option>
                        <option value="0" <?= check_selected($status, 0); ?>> Non Aktif </option>
                      </select>
                   </div>
                   <div class="form-group cent-hidden">
                      <label>ID</label>
                      <input class="form-control" value="<?= $id_user; ?>" name="id" minlength="2" type="text" required>
                    </div>
                <?php }else{
                  $url = base_url('karyawan/tambah_conf');
                }
                ?>
              </div>
            </div>
              <center>
                <button class="btn btn-secondary" type="button" onclick="history.back();"><i class="fa fa-arrow-left"> </i> Kembali </button>
                <input id="submit-btn" class="btn btn-primary" type="submit" value="Submit">
              </center>
            
        </div>
    </div>
    
  </div>
  
</div>
</form>


<script type="text/javascript">
   

   $('.date').datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true
  });

   $('#global-form').submit(function(event) { 
        event.preventDefault(); 
        var values = new FormData(this);
        ajax_with_upload(values,"<?= $url; ?>","","<?= base_url('karyawan'); ?>","div-alert","div");
        return false; //stop
    });

   $( "#email" ).change(function() {
   	  var email = $(this).val();
   	  console.log(email);
   	  get_append_ajax("email="+email, '<?= base_url("user/cek_email"); ?>', "imel", "submit-btn","button");
	});

   $( "#username" ).change(function() {
   	  var username = $(this).val();
   	  console.log(email);
   	  get_append_ajax("username="+username, '<?= base_url("user/cek_username"); ?>', "usrnm", "submit-btn","button");
	});
</script>