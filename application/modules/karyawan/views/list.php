<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
            <div id="div-alert" class="alert alert-warning" style="display: none;"> </div>
            <div class="row" style="padding-bottom: 10px;">
                <div class="col-md-7 cent-left"><h3 class="">Data Karyawan</h3></div>
                <div class="col-md-5 cent-right" id="btn_config">
                    <button class="btn btn-success btn-sm clickable_row_button" id="tambah"  data-toggle="tooltip" data-placement="bottom" title="Tambah"><i class="fa fa-plus"></i></button>
                    <button class="btn btn-info btn-sm cent-hidden clickable_row_button" id="update"  data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-pencil"></i></button>
                    <button class="btn btn-info btn-sm cent-hidden clickable_row_button" id="ktp"  data-toggle="tooltip" data-placement="bottom" title="Lihat KTP"><i class="fa fa-vcard"></i></button>
                    <button class="btn btn-info btn-sm cent-hidden clickable_row_button" id="password" data-toggle="tooltip" data-placement="bottom" title="Edit Password"><i class="fa fa-key"></i></button>
                    <button class="btn btn-danger btn-sm cent-hidden clickable_row_button" id="delete"  data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-close"></i></button>
                </div>
            </div>
              
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table class="table table-hover datatable">
                      <thead>
                        <tr>
                            <th>ID User</th>
                            <th>ID Finger</th>
                            <th>Nama Lengkap</th>
                            <th>Email</th>
                            <th>Username</th>
                            <th>Tanggal Lahir</th> 
                            <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($list as $row) { ?>
                            <tr class="clickable_row" data-id="<?= $row->id_user; ?>" class="menu">
                                <td><?= $row->id_user; ?></td>
                                <td><?= $row->id_finger; ?></td>
                                <td><?= $row->nama_lengkap; ?></td>
                                <td><?= $row->email; ?></td> 
                                <td><?= $row->username; ?></td>
                                <td><?= $row->tgl_lahir; ?></td> 
                                <td><?= active($row->status_akun); ?></td>
                            </tr>
                        <?php }  ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
    </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $("#tambah").click(function(){
        window.location ="<?= base_url('karyawan/form/tambah'); ?>"; 
    });

    $("#password").click(function(){
        get_append_ajax("type=tambah", "<?= base_url('karyawan/get_form_password'); ?>", "","div-alert","div", "yes" , "<i class='fa fa-pencil'></i> Ubah Password") 
    });


    $("#ktp").click(function(){
        get_append_ajax("id="+tr_id, "<?= base_url('karyawan/get_ktp'); ?>", "","div-alert","div", "yes" , "<i class='fa fa-vcard'></i> KTP") 
    });

    $("#delete").click(function(){
           pesan_confirm("Apakah anda yakin?", "Menghapus User Dengan id:"+tr_id, "Ya, Hapus").then((result) => {
            if(result===true){
                simple_ajax('id='+tr_id,"karyawan/delete_conf","","Berhasil!","Gagal!","div-alert","div");
            }
        });
      });

    $("#update").click(function(){
        window.location = "<?= base_url('karyawan/form/update/'); ?>"+tr_id;
      });

  });
</script>