
<form id="global-form">
  <div class="form-group">
    <label>Password Baru</label>
    <input class="form-control" onkeyup="pwdchg()" name="password" minlength="6" type="password" id="pwd1" required>
    <small class="text-success"> Password minimal 6 karakter</small>
  </div>

  <div class="form-group">
    <label>Password Baru</label>
    <input class="form-control" id="pwd2" onkeyup="pwdchg()"  name="password2" minlength="6" type="password" required>
    <small class="text-success"> Masukkan Password yang sama sekali lagi</small>
  </div>
  <center><input id="submit-btn" class="btn btn-primary" type="submit" value="Submit" disabled=""></center>
</form>

<script type="text/javascript">

   function pwdchg(){
      var val1 = $("#pwd1").val();
      var val2 = $("#pwd2").val();

      if(val1=="" || val2==""){
        $("#submit-btn").attr("disabled","disabled");
      }else if(val1==val2){
        $("#submit-btn").removeAttr("disabled","disabled");
      }else{
        $("#submit-btn").attr("disabled","disabled");
      }
   }
   $('#global-form').submit(function(event) { 
        event.preventDefault(); 
        var values = $(this).serialize();
        simple_ajax(values+"&session=true","<?= base_url('user/password_conf'); ?>","","Berhasil!","Gagal!","submit-btn","button");
        return false; //stop
    });
</script>