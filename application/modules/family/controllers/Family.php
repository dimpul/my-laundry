<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Family extends CI_Controller {

	public function __construct()
    {
	    parent::__construct();
      $this->load->library('password');
      $this->load->model('keuangan_laundry/m_keuangan_laundry');
    }


    public function index()
    {
      #Bagian Inisialisasi  
      #Bagian Proses 
      #Bagian Return  
      show_404();
    }

    #Pengeluaran Lainya-----------------------------------------------------------
    public function pengeluaran()
    {
      #Bagian Inisialisasi 
      #Bagian Proses
        $where = array('insert_by'=>get_session('id_user'));
        $data_pemasukkan = h_crud_get_data('pengeluaran_family_view', $where);

      //untuk graphic max 30 data
      $data_graphic     = $this->m_keuangan_laundry->get_graphic_penggunaan_uang_laundry();
      $total_penggunaan = $this->m_keuangan_laundry->total_penggunaan_dana_laundry();

      #Bagian Return 
        $data['data_graphic']     = $data_graphic;  
        $data['total_penggunaan'] = $total_penggunaan;  
        $data['data']   = $data_pemasukkan; 
        load_view('list_pengeluaran',$data,'List Pemasukkan Lainya');
    }
 
    public function get_form_pengeluaran()
    {  
      #Bagian Inisialisasi
         $type = post('type');
         $id   = post('id');

      #Bagian Proses
         if($type=="update"){
            $data_checking_empty = array($id);
            checking_empty($data_checking_empty);
            $where      = array("id"=>$id);
            $data_pemasukkan  = h_crud_get_1_data("pengeluaran_family_view",$where);

            if(empty($data_pemasukkan)){
               echo "<center>Data Tidak Ditemukan</center>";
               return 0;
            }
         }else{
            $data_pemasukkan  = null;
         }


      #Bagian Return
         $data['data'] = $data_pemasukkan;
         $data['type'] = $type;
         load_empty_view('form_pengeluaran',$data);
    }

    function tambah_pengeluaran_conf(){
        #Bagian Inisialisasi
          check_ajax_request();
          $this->db->trans_start();

          $tanggal    = post('tanggal');
          $jumlah     = post('jumlah');
          $keterangan = post('keterangan');

          $data_checking_empty = array($tanggal,$jumlah,$keterangan);
          checking_empty($data_checking_empty);

        #Bagian Proses 
          $data = array(
            "tanggal"=>$tanggal,
            "jumlah"=>$jumlah,
            "keterangan"=>$keterangan,
            "insert_by"=>get_session('id_user')
          );
          $execute = h_crud_tambah('pengeluaran_family_tb', $data);

        #Bagian Return
          if($execute){
            $this->db->trans_complete();
            return_ajax(1,"Berhasil Menambah Pengeluaran");
          }else{
            return_ajax(0,"Gagal Menambah Pengeluaran");
          }
    }

    public function update_pengeluaran_conf(){
       #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();
        $id         = post('id');
        $tanggal    = post('tanggal');
        $jumlah     = post('jumlah');
        $keterangan = post('keterangan');

        $data_checking_empty = array($tanggal,$jumlah,$keterangan);
        checking_empty($data_checking_empty);

      #Bagian Proses 
        //eksekusi guys
        $data = array(
            "tanggal"=>$tanggal,
            "jumlah"=>$jumlah,
            "keterangan"=>$keterangan
          );
        $execute = h_crud_update('pengeluaran_family_tb',$data,'id',$id);
        
      #Bagian Return
        if($execute){
          $this->db->trans_complete();
          return_ajax(1,"Berhasil Mengubah Data Pengeluaran");
        }else{
          return_ajax(0,"Gagal Mengubah Data Pengeluaran");
        }
        echo json_encode($return);
    }

    public function delete_pengeluaran_conf()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();
        $id             = $this->input->post('id');
        $data_checking_empty = array($id);
        checking_empty($data_checking_empty);

      #Bagian Proses  
        $execute = h_crud_delete('pengeluaran_family_tb','id',$id);
        
      #Bagian Return
        if(!$execute){
          return_ajax(0,"Gagal Menghapus Data Pengeluaran");
        }else{
          $this->db->trans_complete();
          return_ajax(1,"Berhasil Menghapus Data Pengeluaran");
        }
    }
    #End Pengeluaran Lainya-------------------------------------------------------
}