<link rel="stylesheet" href="<?= base_url('assets/vendors/chartjs/dist/Chart.css'); ?>">
<script src="<?= base_url('assets/vendors/chartjs/dist/Chart.js'); ?>"></script> 
    
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
            <div id="div-alert" class="alert alert-warning" style="display: none;"> </div>
            <div class="row" style="padding-bottom: 10px;">
                <div class="col-md-8 cent-left"><h3 class="">Graph Penggunaan Uang Laundry</h3></div> 
            </div> 
                <div class="col-12">
                   <canvas id="myChart"  height="100"></canvas>
                </div> 
            </div>
          </div>
    </div> 
    <br>


<div class="row">
     <div class="col-md-4">
        <div class="card text-white bg-danger">
            <div class="card-body" style="text-align: center; ">
                <h4> Total Penggunaan Dana</h4>
                <h1 style="padding-top: 5px;"> <?= rupiah($total_penggunaan,"Rp. "); ?></h1>
            </div>
        </div>
    </div>

    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
            <div id="div-alert" class="alert alert-warning" style="display: none;"> </div>
            <div class="row" style="padding-bottom: 10px;">
                <div class="col-md-8 cent-left"><h3 class="">List Penggunaan Uang Laundry</h3></div>
                <div class="col-md-4 cent-right" id="btn_config">
                    <button class="btn btn-success btn-sm clickable_row_button" id="tambah"  data-toggle="tooltip" data-placement="bottom" title="Tambah"><i class="fa fa-plus"></i></button>
                    <button class="btn btn-warning btn-sm cent-hidden clickable_row_button" id="update"  data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-pencil"></i></button>
                    <button class="btn btn-danger btn-sm cent-hidden clickable_row_button" id="delete"  data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-close"></i></button>
                </div>
            </div>
              
              <div class="row"> 
                <div class="col-12">
                  <div class="table-responsive" id="tbl">
                     <table class="table table table-hover datatable">
                      <thead>
                        <tr>
                            <th>ID</th>
                            <th>Tanggal</th>
                            <th>Jumlah</th>
                            <th>Keterangan</th>
                            <th>Updated By</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($data as $row) { ?>
                            <tr class="clickable_row" data-id="<?= $row->id; ?>" class="menu">
                                <td><?= $row->id; ?></td>
                                <td><?= $row->hari.", ".tgl_indo($row->tanggal); ?></td>
                                <td><?= rupiah($row->jumlah,"Rp. "); ?></td>
                                <td><?= $row->keterangan; ?></td> 
                                <td><?= $row->nama_lengkap; ?></td> 
                            </tr>
                        <?php }  ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
    </div>
</div>

<script type="text/javascript"> 
  $(document).ready(function(){
    $("#tambah").click(function(){
        get_append_ajax("type=tambah", "<?= base_url('family/get_form_pengeluaran'); ?>", "","div-alert","div", "yes" , "<i class='fa fa-plus'></i> Tambah Pengeluaran");
    });


    $("#delete").click(function(){
           pesan_confirm("Apakah anda yakin?", "Menghapus Data Pengeluaran Dengan id:"+tr_id, "Ya, Hapus").then((result) => {
            if(result===true){
                simple_ajax('id='+tr_id,"<?= base_url('family/delete_pengeluaran_conf'); ?>","","Berhasil!","Gagal!","div-alert","div");
            }
        });
      });

    $("#update").click(function(){
        get_append_ajax("type=update&id="+tr_id, "<?= base_url('family/get_form_pengeluaran'); ?>", "","div-alert","div", "yes" , "<i class='fa fa-pencil'></i> Edit Data Pemasukkan Lainya") 
      });

  });
</script>

<script>
var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: JSON.parse('<?= $data_graphic["label"]; ?>'),
        datasets: [{
            label: 'Total Pengeluaran',
            data: JSON.parse('<?= $data_graphic["data"]; ?>'),
            backgroundColor: [
                'rgba(252, 3, 107, 0.2)'
            ],
            borderColor: [
                'rgba(252, 3, 107, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>