<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
    {
	    parent::__construct();
		check_session();
    }

    public function original()
    {
       $this->load->view('main_original');
    }

    public function index()
    {
       load_view('main');
    }
}