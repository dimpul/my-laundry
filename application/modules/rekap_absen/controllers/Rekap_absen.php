<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap_absen extends CI_Controller {

	public function __construct()
    {
	    parent::__construct();
      $this->load->model('karyawan/m_karyawan');
      $this->load->model('user/m_user');
      $this->load->model('m_rekap_absen');
    }


    public function index()
    {
      #Bagian Inisialisasi        
      #Bagian Proses
        $data_karyawan = $this->m_karyawan->get_karyawan();
        
      #Bagian Return
        $data['list']   = $data_karyawan;
        load_view('list',$data,'List Data Rekap Absen Karyawan');
    }

    public function get_form_jadwal()
    {  
      #Bagian Inisialisasi
        check_ajax_request();
         $id   = post('id');

      #Bagian Proses
         $where = array("id_user"=>$id);
         $data_user = h_crud_get_1_data('user_tb',$where); 

      #Bagian Return
         $data['data'] = $data_user;
         load_empty_view('form_jadwal',$data);
    }

    public function get_form_detail(){
      #Bagian Inisialisasi
       check_ajax_request(); 
       $id_absen = post('id_absen');

       $data_checking_empty = array($id_absen);
       checking_empty($data_checking_empty,false);

      #Bagian Proses
       //get data absen
       $data_absen = h_crud_get_1_data("log_absen_tb",array('id_absen'=>$id_absen));

       //get data jadwal kerja
       $where = array('id_user'=>$data_absen->id_user,'tanggal'=>$data_absen->tanggal);
       $data_jk = h_crud_get_1_data("jadwal_kerja_tb",$where);
       
       //hitung jam kerja dalam sehari
       $date1=date_create("$data_absen->tanggal $data_absen->in");
       $date2=date_create("$data_absen->tanggal $data_absen->out");
       $diff=date_diff($date1,$date2);
       $total_jk = "Tidak ada";
       if($diff->h !== 0){
          $total_jk = $diff->h." Jam ";
       }

       if($diff->i !== 0){
          $total_jk .= $diff->i." Menit ";
       }

       //hitung terlambat
       $arr_terlambat = explode(":", $data_absen->total_terlambat);
       $terlambat = "";
       $terlambat_jam = "";
       $terlambat_menit = "";
       if($arr_terlambat[0] !== '00'){
          $terlambat_jam = $arr_terlambat[0]." Jam ";
       } 
       if($arr_terlambat[1] !== '00'){
          $terlambat_menit = (int)$arr_terlambat[1]." Menit ";
       } 
       $terlambat = $terlambat_jam.$terlambat_menit;

       //hitung pulang cepat
       $arr_pc = explode(":", $data_absen->total_pulang_cepat);
       $pc = "";
       $pc_jam = "";
       $pc_menit = "";
       if($arr_pc[0] !== '00'){
          $pc_jam = $arr_pc[0]." Jam ";
       }

       if($arr_pc[1] !== '00'){
          $pc_menit = (int)$arr_pc[1]." Menit ";
       }

       $pc = $pc_jam.$pc_menit;

       //cek apakah mengajukan OT atau nggak
       $where = array('id_user'=>$data_absen->id_user,'tgl_ot'=>$data_absen->tanggal);
       $overtime = h_crud_get_1_data("overtime_tb",$where);
       if(empty($overtime)){
          $ot_status = '<label class="badge badge-danger">Tidak</label>';
       }else{
          $ot_status = '<label class="badge badge-success">Iya</label>';
       }

      #Bagian Return
       $data['tanggal']         = $data_absen->tanggal;
       $data['data_absen']      = $data_absen;
       $data['jadwal_kerja']    = $data_jk;
       $data['total_jam_kerja'] = $total_jk;
       $data['ot_status'] = $ot_status;
       $data['terlambat'] = $terlambat;
       $data['pulang_cepat'] = $pc;
       load_empty_view('form_detail',$data);
    }

     public function get_form_detail_2(){
      #Bagian Inisialisasi
       check_ajax_request(); 
       $id_absen = post('id_absen');

       $data_checking_empty = array($id_absen);
       checking_empty($data_checking_empty,false);

      #Bagian Proses
       //get data absen
       $data_absen = h_crud_get_1_data("log_absen_tb",array('id_absen'=>$id_absen));

       //get data jadwal kerja
       $where = array('id_user'=>$data_absen->id_user,'tanggal'=>$data_absen->tanggal);
       $data_jk = h_crud_get_1_data("jadwal_kerja_tb",$where);
       if(empty($data_jk)){
          $off = '<label class="badge badge-danger">Tidak</label>';
       }else{
          $off = '<label class="badge badge-success">Iya</label>';
       }
       
       //hitung jam kerja dalam sehari
       $date1=date_create("$data_absen->tanggal $data_absen->in");
       $date2=date_create("$data_absen->tanggal $data_absen->out");
       $diff=date_diff($date1,$date2);
       $total_jk = "Tidak ada";
       if($diff->h !== 0){
          $total_jk = $diff->h." Jam ";
       }

       if($diff->i !== 0){
          $total_jk .= $diff->i." Menit ";
       }

       //hitung terlambat
       $arr_terlambat = explode(":", $data_absen->total_terlambat);
       $terlambat = "";
       $terlambat_jam = "";
       $terlambat_menit = "";
       if($arr_terlambat[0] !== '00'){
          $terlambat_jam = $arr_terlambat[0]." Jam ";
       } 
       if($arr_terlambat[1] !== '00'){
          $terlambat_menit = (int)$arr_terlambat[1]." Menit ";
       } 
       $terlambat = $terlambat_jam.$terlambat_menit;

       //hitung pulang cepat
       $arr_pc = explode(":", $data_absen->total_pulang_cepat);
       $pc = "";
       $pc_jam = "";
       $pc_menit = "";
       if($arr_pc[0] !== '00'){
          $pc_jam = $arr_pc[0]." Jam ";
       }

       if($arr_pc[1] !== '00'){
          $pc_menit = (int)$arr_pc[1]." Menit ";
       }

       $pc = $pc_jam.$pc_menit;

       //cek apakah mengajukan OT atau nggak
       $where = array('id_user'=>$data_absen->id_user,'tgl_ot'=>$data_absen->tanggal);
       $overtime = h_crud_get_1_data("overtime_tb",$where);
       if(empty($overtime)){
          $ot_status = '<label class="badge badge-danger">Tidak</label>';
       }else{
          $ot_status = '<label class="badge badge-success">Iya</label>';
       }

      #Bagian Return
       $data['tanggal']         = $data_absen->tanggal;
       $data['data_absen']      = $data_absen;
       $data['jadwal_kerja']    = $data_jk;
       $data['total_jam_kerja'] = $total_jk;
       $data['ot_status'] = $ot_status;
       $data['off_status'] = $off;
       $data['terlambat'] = $terlambat;
       $data['pulang_cepat'] = $pc;
       load_empty_view('form_detail_2',$data);
    }

    public function get_form_manage_jadwal()
    {  
      #Bagian Inisialisasi
        check_ajax_request();
         $id   = post('id_user');
         $tanggal   = post('tanggal');
         $id_jadwal = post('id_jadwal');

         $data_checking_empty = array($id);
         checking_empty($data_checking_empty,false);

      #Bagian Proses
         if(!empty($id_jadwal)){
           $data_checking_empty = array($id_jadwal);
           checking_empty($data_checking_empty,false);

           $where = array('id'=>$id_jadwal); 
           $data_jadwal = h_crud_get_1_data('jadwal_kerja_tb',$where);
           $tanggal = $data_jadwal->tanggal; 
         }else{
           $data_checking_empty = array($tanggal);
           checking_empty($data_checking_empty,false);

           $data_jadwal = array();
         }
         //get master absen
         $data_master_absen = h_crud_get_data("master_absen_tb",array(),'jam_masuk');

      #Bagian Return
         $data['data_jadwal']  = $data_jadwal;
         $data['master_absen'] = $data_master_absen;
         $data['tanggal'] = $tanggal;
         load_empty_view('form_manage_jadwal',$data);
    } 

    public function get_form_generate()
    {  
      #Bagian Inisialisasi
         check_ajax_request();
      #Bagian Proses
         $data_master_absen = h_crud_get_data("master_absen_tb",array(),'jam_masuk');
      #Bagian Return 
         $data['master_absen'] = $data_master_absen; 
         load_empty_view('form_generate_jadwal',$data);
    } 

    public function get_form_tanggal()
    {  
      #Bagian Inisialisasi
         check_ajax_request();
      #Bagian Proses
      #Bagian Return  
         load_empty_view('form_tanggal');
    } 

    public function detail($id_user,$tanggalx){
      #Bagian Inisialisasi  
        //check if null
        if(empty($id_user) || empty($tanggalx)){
          set_flashsession('<i class="fa fa-close"> </i> Data tidak ditemukan');
          redirect(base_url('Jadwal_kerja'));  
        } 

        //check user
        $where      = array("id_user"=>$id_user);
        $data_user  = h_crud_get_1_data("user_tb",$where);
        if(empty($data_user)){
          set_flashsession('<i class="fa fa-close"> </i> Data tidak ditemukan');
          redirect(base_url('Jadwal_kerja'));  
        } 

      #Bagian Proses
        //change date format yang bisa digunakan untuk query
        $date    = date_create("01-".$tanggalx);
        $tanggal = date_format($date,"Y-m"); 
        $date_next = date_create("01-".$tanggalx);
        $nextmonth = date_add($date_next, date_interval_create_from_date_string('1 month')); 
        $nextmonth = date_format($date_next, 'm-Y');

        //get all date in month
        $day_in_month = array();
        $tanggal_awal = date('Y-m-d', strtotime("01-".$tanggalx));
        $tanggal_akhir = new DateTime( '01-'.$tanggalx );
        $tanggal_akhir = $tanggal_akhir->modify( '+1 month' );
        $tanggal_akhir = $tanggal_akhir->format( 'Y-m-d' );  
        $period = new DatePeriod(
             new DateTime($tanggal_awal),
             new DateInterval('P1D'),
             new DateTime($tanggal_akhir)
        );
        foreach ($period as $key => $value) {
          $tanggal = $value->format('Y-m-d');  
          array_push($day_in_month, $tanggal);
        } 
        $date_prev = date_create("01-".$tanggalx);
        $prevmonth = date_add($date_prev, date_interval_create_from_date_string('-1 month')); 
        $prevmonth = date_format($date_prev, 'm-Y');

      #get finger_absen untuk dimasukkan kedalam table
        $tanggal_xyz = date_format($date,"Y-m"); 
        $data_rekap = $this->m_rekap_absen->get_all_rekap($id_user, $tanggal_xyz);  

      #count jumlah hari kerja 
        $count_rekap = $this->m_rekap_absen->get_detail_masuk($id_user, $tanggal_xyz);  

      #Bagian Return 
        $data['next']         = base_url('rekap_absen/detail/').$id_user.'/'.$nextmonth;
        $data['prev']         = base_url('rekap_absen/detail/').$id_user.'/'.$prevmonth;
        $data['data_user']    = $data_user;
        $data['tgl']          = tahun_bulan($tanggal);
        $data['calendar_date'] = $tanggal;
        $data['date_in_month'] = json_encode($day_in_month);
        $data['data_rekap']    = $data_rekap;
        $data['count_rekap']    = $count_rekap;
        load_view('rekap_absen',$data,"Set Jadwal Kerja ".$data_user->nama_lengkap,'false');
    }

    public function get_event(){

      #Bagian Inisialisasi
        check_ajax_request();
        $tanggal       = post('tanggal');
        $id_user       = post('id_user');
        $tanggal2      = substr($tanggal, 0,7);
      #Bagian Proses
        //get data jadwal kerja
        $rekap_absen = $this->m_rekap_absen->get_event($tanggal2,$id_user);  
        $event = array();
        foreach ($rekap_absen as $row) {
          if($row->keterangan=="Masuk"){
            $color = "green";
          }elseif($row->keterangan=="Alfa"){
            $color = "red";
          }elseif($row->keterangan=="OFF"){
            $color = "blue";
          }else{ 
            $color = "#ff8c00";
          }
          
          $tmp_array = array(
            "id" => $row->id_absen,
            "title" => $row->keterangan, 
            "start" => $row->tanggal."T".$row->in,
            "end" => $row->tanggal."T".$row->out,
            "color" => $color
          );
          array_push($event, $tmp_array);
        }
        echo json_encode($event);
    }

     public function add_jadwal_kerja()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();

        $tanggal       = post('tanggal');
        $master_absen  = post('master_absen');
        $id_user       = post('id_user');

        $data_checking_empty = array($tanggal,$id_user,$master_absen);
        checking_empty($data_checking_empty,false);
        
      #Bagian Proses
        //hapus
        $this->m_jadwal_kerja->delete_jadwal($tanggal, $id_user);

        //get masterabsen
        $where = array('id_master_absen'=>$master_absen);
        $data_master_absen = h_crud_get_1_data('master_absen_tb',$where);

        //tambah
        $data = array(
          "id_user"=>$id_user,
          "tanggal"=>$tanggal,
          "jam_masuk"=>$data_master_absen->jam_masuk,
          "jam_pulang"=>$data_master_absen->jam_pulang
        );
        $execute = h_crud_tambah('jadwal_kerja_tb', $data);

      #Bagian Return
        if($execute){
          $this->db->trans_complete();
          return_ajax(1,"Berhasil Set Jadwal Kerja");
        }else{
          return_ajax(0,"Gagal Set Jadwal Kerja");
        }
    }

    function eksekusi_rekap_absen($data){ 
          $execute = h_crud_tambah('log_absen_tb', $data);

           #Bagian Return
          if($execute){
            $this->db->trans_complete();
            //echo $data['tanggal']." Berhasil Generate Rekap Absen";
          }else{
            echo $data['tanggal']." Gagal Generate Rekap Absen";
          }
    }

    public function generate_rekap_absen()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();

        $tanggal    = post('date'); 
        $id_user    = post('id_user');
        $id_finger  = post('id_finger');

        /*$tanggal    = '2019-12-10'; 
        $id_user    = '40';
        $id_finger  = '2';*/

        $data_checking_empty = array($tanggal,$id_user,$id_finger);
        checking_empty($data_checking_empty,false);
        
      #Bagian Proses
          $where = array('id_user'=>$id_user,'tanggal'=>$tanggal);

          //hapus log absen hari itu
            h_crud_delete_ver2("log_absen_tb",$where); 

          //get jadwal kerja
           $jadwal_kerja = h_crud_get_1_data("jadwal_kerja_tb",$where);
           if(empty($jadwal_kerja)){
              echo $tanggal." Tidak memiliki Jadwal Kerja";
              return 0;
           }
           $jadwal_masuk  = $jadwal_kerja->jam_masuk;
           $jadwal_pulang = $jadwal_kerja->jam_pulang; 

           //off
            if($jadwal_masuk=='00:00:00' && $jadwal_pulang=='00:00:00'){
              $keterangan = 'OFF';
              $data = array(
                "id_user"=>$id_user,
                "tanggal"=>$tanggal,
                "in"=>'00:00:00',
                "out"=>'00:00:00',
                "total_terlambat"=>'00:00:00',
                "total_pulang_cepat"=>'00:00:00',
                "keterangan"=>$keterangan
              );
              $this->eksekusi_rekap_absen($data); 
              return 0;
            }

          //get log finger , first and last
           $log_finger = $this->m_rekap_absen->get_log_first_last($id_finger,$tanggal);
           $log_masuk  = $log_finger->absen_datang;
           $log_pulang = $log_finger->absen_pulang;
           $keterangan = 'Masuk';
 
           if(empty($log_masuk) && empty($log_pulang)){
              $keterangan = 'Alfa';
              $data = array(
                "id_user"=>$id_user,
                "tanggal"=>$tanggal,
                "in"=>'00:00:00',
                "out"=>'00:00:00',
                "total_terlambat"=>'00:00:00',
                "total_pulang_cepat"=>'00:00:00',
                "keterangan"=>$keterangan
              );
              $this->eksekusi_rekap_absen($data);
              return 0;
           }

           //hitung terlambat
           $second_1 = time_to_second($jadwal_masuk);
           $second_2 = time_to_second($log_masuk);
           $second_telambat = $second_2-$second_1;
           if($second_telambat <= 0){
              $terlambat = '00:00:00';
           }else{
              $terlambat = second_to_time($second_telambat);
           }

           //hitung pulang cepat
           $second_1 = time_to_second($jadwal_pulang);
           $second_2 = time_to_second($log_pulang);
           
           
           $second_pc = $second_1-$second_2;
           if($second_pc <= 0){
              $pulang_cepat = '00:00:00';
           }else{
              $pulang_cepat = second_to_time($second_pc);
           }

           //keterangan
           if($second_telambat > 0 && $second_pc > 0){
              $keterangan = "Terlambat & Pulang Cepat";
           }elseif($second_telambat > 0){
              $keterangan = "Terlambat";
           }elseif($second_pc > 0){
              $keterangan = "Pulang Cepat";
           }


          //tambah 
          $data = array(
            "id_user"=>$id_user,
            "tanggal"=>$tanggal,
            "in"=>$log_masuk,
            "out"=>$log_pulang,
            "total_terlambat"=>$terlambat,
            "total_pulang_cepat"=>$pulang_cepat,
            "keterangan"=>$keterangan
          );
          $this->eksekusi_rekap_absen($data);
          return 0;
    } 

    
}