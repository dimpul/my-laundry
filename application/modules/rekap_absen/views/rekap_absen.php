<!-- Full Calendar Io -->
<link href='<?= base_url('assets/vendors/fullcalendar/packages/core/main.css'); ?>' rel='stylesheet' />
<link href='<?= base_url('assets/vendors/fullcalendar/packages/daygrid/main.css'); ?>' rel='stylesheet' />
<script src='<?= base_url('assets/vendors/fullcalendar/packages/core/main.js'); ?>'></script>
<script src='<?= base_url('assets/vendors/fullcalendar/packages/interaction/main.js'); ?>'></script>
<script src='<?= base_url('assets/vendors/fullcalendar/packages/daygrid/main.js'); ?>'></script>
<script src='<?= base_url('assets/vendors/fullcalendar/packages/timegrid/main.js'); ?>'></script>

<style type="text/css">
  .custom1{
    border-radius: 10px !important;
  }
   .fc-day-grid-event .fc-content {
     white-space: normal; 
  }
  .btn{
    margin-bottom: 5px;
  }
</style>



<div class="row mb-4">
    <div class="col-md-5 col-xs-12 mt-2" style="font-weight: bold;">
      <div class="card bg bg-primary" style="padding-bottom: 0px; color: white; "> 
        <div class="card-body" style="padding-bottom: 0px; padding-top: 15px;  "> 

    
        <h4> <b>Nama</b> : <?= $data_user->nama_lengkap; ?> </h4>
        <h4> <b>Periode</b> : <?= $tgl; ?></h4> <br> 
      </div>
    </div>
  </div>
    <div class="col-md-7 col-xs-12 mt-2" style="font-weight: bold;">
      <div class="card " style="padding-bottom: 0px;  text-align: center; border-style: solid !important; border-color: #4d7cff !important;"> 
        <div class="card-body" style="padding-bottom: 20px; padding-top: 15px;"> 
          <!-- <button class="custom1 btn  btn-primary" id="generate"> <i class="fa fa-cog fa-spin"></i> Generate </button> -->
          <button class="custom1 btn  btn-primary" onclick="window.location ='<?= base_url(); ?>rekap_absen'"> <i class="fa fa-home"></i> Kembali </button>
          <button class="custom1 btn  btn-success" id="pilih"> <i class="fa fa-calendar"></i> Pilih Tanggal </button>
          <a class="custom1 btn  btn-warning" href="<?= $prev; ?>"> <i class="fa fa-arrow-left"></i> Sebelumnya </a>
          <a class="custom1  btn btn-info" href="<?= $next; ?>"> <i class="fa fa-arrow-right"></i> Berikutnya </a>
        </div>
    </div>
  </div>
</div>

<div id="div-alert" class="alert alert-warning" style="display: none;"> </div>

 
<div class="card "> 
  <div class="card-body"> 
      <center><h2> Rekap Kalender </h2></center>
      <div class="card" id="card-progress" style="display: none;">
          <div class="card-body">
              <h3><i class="fa fa-spin fa-spinner"></i> <div id="lblprgs" style="display: inline-block;">0</div>%
              Menyimpan Log Finger</h3>
              <br>
              <div class="progress" style="height: 40px;">
                <div id="prgs" class="progress-bar progress-bar-striped bg-success progress-bar-animated" role="progressbar"  style="width: 5%"></div>
              </div>
              <div class="cent-hidden" id="ulang" style="padding-top: 10px;"><center><button class="btn btn-success" onclick="location.reload();"> <i class="fa fa-plus"></i> Upload Lagi </button></center></div>
          </div>
      </div>

      <div id="calendar" class="mt-4"> </div>
  </div>
</div>



<br><br>
<div class="card"> 
  <div class="card-body"> 
    <center><h2> Rekap Absen </h2></center> 
    <br>
    <div class="row">
      <div class="col-md-3 col-xs-12 mb-4">
         <table class="table table-sm table-striped table-bordered">
           <tr>
             <td> <h5 style="font-weight: bold;">Total Jam Kerja</h5> </td> 
           </tr>
           <tr style="text-align: center;">
             <td id="jam_kerja"> - </td> 
           </tr>
           <tr>
             <td> <h5 style="font-weight: bold;">Total Hari Masuk</h5> </td> 
           </tr>
           <tr style="text-align: center;">
             <td> <h4> <?php if(!empty($count_rekap['Masuk'])){echo $count_rekap['Masuk']; }else{echo 0;} ?> Hari</h4> </td> 
           </tr>
           <tr>
             <td> <h5 style="font-weight: bold;">Total Hari Alfa</h5> </td> 
           </tr>
           <tr style="text-align: center;">
             <td> <h4> <?php if(!empty($count_rekap['Alfa'])){echo $count_rekap['Alfa']; }else{echo 0;} ?> Hari</h4> </td> 
           </tr>
         </table>
      </div>
      <div class="col-md-9 col-xs-12">
              <table class="table datatable table-striped">
                <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Jam Masuk</th>
                    <th>Jam Pulang</th>
                    <th>Total Jam Kerja</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $sum_minute = 0;
                foreach ($data_rekap as $key => $value) { 
                  #hitung jam kerja
                   $date1=date_create("$value->tanggal $value->in");
                   $date2=date_create("$value->tanggal $value->out");
                   $diff=date_diff($date1,$date2);
                   $total_jk = "Tidak ada";
                   if($diff->h !== 0){
                      $total_jk = $diff->h." Jam ";
                      $sum_minute += ($diff->h*60);
                   }

                   if($diff->i !== 0){
                      $total_jk .= $diff->i." Menit ";
                      $sum_minute += $diff->i;
                   }
                ?>
                <tr>
                  <td><?= $value->tanggal; ?></td>
                  <td><?= $value->in; ?></td>
                  <td><?= $value->out; ?></td>
                  <td><?= $total_jk; ?></td>
                </tr>
                <?php }  ?>
                </tbody>
              </table> 
              <?php
                #count hour minute
                $t = $sum_minute;
                $h = floor($t/60) ? floor($t/60) .' Jam' : '';
                $m = $t%60 ? $t%60 .' Menit' : '';
                $jumlah_jam_kerja =  $h && $m ? $h.' '.$m : $h.$m;
              ?>
              <script type="text/javascript">
                $("#jam_kerja").empty();
                $("#jam_kerja").append('<h4><?= $jumlah_jam_kerja; ?></h4>');
              </script>
      </div>
    </div> 
  </div>
</div>

<div class="modal fade" id="GlobalModal" role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="GlobalModalTitle">Title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> 
      </div>
      <div class="modal-body" id="GlobalModalBody"></div>
    </div>
  </div> 
</div>
    

<script type="text/javascript">
  id_user = "<?= $data_user->id_user; ?>";
  var jsonEvent = null;
  var calendarEl = document.getElementById('calendar');

  function get_form(arg){
    //get and split date and time
    var tmp = arg.split("T");
    var time = "";
    var datetime = "";
    var date = "";

    var date = tmp[0];
    console.log(date);
    
    get_append_ajax("id_user="+id_user+"&tanggal="+date+"&id_jadwal=", "<?= base_url('jadwal_kerja/get_form_manage_jadwal'); ?>", "","div-alert","div", "yes" , "<i class='fa fa-pencil'></i> Set Jadwal Kerja");
  }

  function get_absen(arg){ 
    var id = arg['event']['id']; //id terapi
    console.log(id);
    get_append_ajax("id_user="+id_user+"&tanggal="+"&id_absen="+id, "<?= base_url('rekap_absen/get_form_detail_2'); ?>", "","div-alert","div", "yes" , "<i class='fa fa-calendar-check-o'></i> Detail Absen");
  
  }

  generate_calendar();
  function generate_calendar(){
    $.ajax({
        type: 'POST',
        url:'<?= base_url('rekap_absen/get_event'); ?>',
        data: "tanggal=<?= $calendar_date; ?>&id_user=<?= $data_user->id_user; ?>",
        beforeSend: function(){

          $("#calendar").empty();
          pesan_tunggu("Memuat Kalender, Mohon Tunggu . . .","div-alert","div");
        },
        success:function(data){
          reset_pesan_tunggu("div-alert","div");
          jsonEvent = JSON.parse(data);
          build_calendar();
        }
    });
  }

  function build_calendar(){
    var calendar = new FullCalendar.Calendar(calendarEl, {
      defaultDate: "<?= $calendar_date; ?>",
      locale: 'id',
      plugins: [ 'interaction', 'dayGrid', 'timeGrid' ], 
      header: {
        left:'title',
        center: '',
        right:'dayGridMonth',
        //right:'title'
      },
      navLinks: true, // can click day/week names to navigate views
      selectable: true,
      selectMirror: true,
      disableDragging: true, 
      eventClick: function(arg) {
        //event yg di klik
          get_absen(arg);
      },
      agendaEventMinHeight: 100,
      editable: false ,
      eventLimit: true, // allow "more" link when too many events
      events: jsonEvent,
      allDaySlot: false,
      eventTextColor: "white",
      displayEventTime: false,
      eventTimeFormat: {
        hour: 'numeric',
        minute: '2-digit',
        meridiem: false
      }
    });

    calendar.render();
  }
</script>
<script type="text/javascript">
   var month_year    = "<?= $calendar_date; ?>";
   var date_in_month = JSON.parse('<?= $date_in_month; ?>');
    var jumlah_row = 0;
    var jumlah_submit = 0;

  $("#pilih").click(function(){
      get_append_ajax("","<?= base_url('rekap_absen/get_form_tanggal'); ?>", "","div-alert","div", "yes" , "<i class='fa fa-calendar '></i> Pilih Tanggal");
  });

    function checking(i){
      //console.log(i);
        if(jumlah_row==i){ 
            setTimeout(function(){ 
              $("#card-progress").fadeToggle('slow');
              $("#calendar").fadeToggle('fast');
              $("#generate").empty();
              $("#generate").append('<i class="fa fa-spin fa-cog"></i> Generate');
              $("#generate").removeAttr('disabled');
              generate_calendar();
            }, 1500);
             
        }
    }

  var i = 0;
  $("#generate").click(function(){
    $("#lblprgs").empty();
    $("#lblprgs").append(0);
    $("#generate").empty();
    $("#generate").append('<i class="fa fa-spin fa-spinner"></i> Mohon Tunggu . . .');
    $("#generate").attr('disabled','disabled');


    jumlah_submit = 0; 
    $("#prgs").css("width","0%");
    $("#card-progress").fadeToggle('fast');
    $("#calendar").fadeToggle('slow');
    i = 0;
    jumlah_row = date_in_month.length;   
    $.each(date_in_month,async function( key, value ) {
          const result = await fromServer(value);
          console.log(result);
          i++;
          checking(i);
      });
    });

  function fromServer(value){
      return new Promise((resolve, reject) => {
         $.ajax({
            type: 'POST',
            url:'<?= base_url('rekap_absen/generate_rekap_absen'); ?>',
            data: "date="+value+"&id_user=<?= $data_user->id_user; ?>&id_finger=<?= $data_user->id_finger; ?>",
            success:function(data){
                jumlah_submit++;
                var tmp_persen = (jumlah_submit/jumlah_row)*100;
                var persen = tmp_persen.toFixed(0);
                $("#prgs").css("width",persen+"%");
                $("#lblprgs").empty();
                $("#lblprgs").append(persen);
                resolve(data);
            }
        });
      });
    }

</script>