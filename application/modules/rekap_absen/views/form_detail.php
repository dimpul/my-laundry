<div class="row">
	<div class="col-md-6" style="margin-bottom: 10px;">
		<center> <h3> Jadwal Kerja </h3></center> <br>
		<table class="table table-hover table-striped">
			<tr> 
				<th> Jadwal Masuk </th>
				<td> <?= $jadwal_kerja->jam_masuk; ?> </td>
			</tr>
			<tr> 
				<th> Jadwal Pulang </th>
				<td> <?= $jadwal_kerja->jam_pulang; ?> </td>
			</tr>
			<tr> 
				<th> Overtime </th>
				<td> <?= $ot_status; ?> </td>
			</tr>
		</table>
	</div>
	<div class="col-md-6" >
		<center> <h3> Log Finger </h3></center> <br>
		<table class="table table-hover table-striped">
			<tr> 
				<th> Log Masuk </th>
				<td> <?= $data_absen->in; ?> </td>
			</tr>
			<tr> 
				<th> Log Pulang </th>
				<td> <?= $data_absen->out; ?> </td>
			</tr>
			<tr> 
				<th> Total Jam Kerja </th>
				<td> <?= $total_jam_kerja; ?> </td>
			</tr>

			<?php if($terlambat !== ''){ ?>
				<tr> 
					<th> Terlambat</th>
					<td> <?= $terlambat; ?> </td>
				</tr>
			<?php } ?>

			<?php if($pulang_cepat !== ''){ ?>
				<tr> 
					<th> Pulang Cepat</th>
					<td> <?= $pulang_cepat; ?> </td>
				</tr>
			<?php } ?>

			<tr> 
				<th> Keterangan </th>
				<td> <?= $data_absen->keterangan; ?> </td>
			</tr>
			<?php if($data_absen->total_terlambat){ ?>

			<?php } ?>
		</table>
	</div>
</div>

