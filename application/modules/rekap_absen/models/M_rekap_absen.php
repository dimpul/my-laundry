<?php
class M_rekap_absen extends CI_Model {
   public function __construct()
   {
        parent::__construct();
   }

   public function get_log_first_last($id_finger,$tanggal)
   {
        #check if user exists
        $this->db->select('min(jam) as absen_datang, max(jam) as absen_pulang');
        $this->db->from('log_finger_tb');
        $this->db->where('id_finger',$id_finger);
        $this->db->where('tanggal',$tanggal);
        return $this->db->get()->row();
   }

   public function get_event($tanggal,$id_user)
   {
        #check if user exists
        $this->db->select('*');
        $this->db->from('log_absen_tb');
        $this->db->where('id_user',$id_user);
        $this->db->where('tanggal like',$tanggal."%");
        return $this->db->get()->result();
   }

   public function get_all_rekap($id_user, $month_year){
        $this->db->select('*');
        $this->db->from('log_absen_tb');
        $this->db->where('id_user',$id_user);
        $this->db->where('tanggal like',$month_year.'%');
        $this->db->where('keterangan','Masuk');
        return $this->db->get()->result();

   }

   public function get_detail_masuk($id_user, $month_year){
        $this->db->select('keterangan, count(*) as jumlah_hari');
        $this->db->from('log_absen_tb');
        $this->db->where('id_user',$id_user);
        $this->db->where('tanggal like',$month_year.'%');
        $this->db->group_by('keterangan');
        $data =  $this->db->get()->result();

        $arr = array();
        foreach ($data as $row) {
          $arr[$row->keterangan] = $row->jumlah_hari;
        }
        return $arr;

   }

   
}

?>