<table class="table table table-hover  datatable">
  <thead>
    <tr align="center">
        <th>#</th>
        <th>Tanggal</th>
        <th>Nama Cust.</th>
        <th>Jenis Laundry</th>
        <th>Ukuran</th>
        <th>Harga</th>
        <th>Jumlah</th>
        <th>Total</th>
        <th>Keterangan</th>
        <th>Insert By</th> 
    </tr>
  </thead>
  <tbody>
    <?php $total=0; foreach ($data_pemasukkan as $row) { ?>
        <tr class="clickable_row" data-id="<?php echo $row->id; ?>" class="menu">
            <td><?= $row->id; ?></td>
            <td><?= tgl_indo($row->tanggal); ?></td>
            <td><?= $row->nama_customer ; ?></td> 
            <td><?= $row->jenis_laundry ; ?></td> 
            <td><?php if($row->data_ukuran=='1'){echo $row->ukuran;}else{echo '-';} ?></td> 
            <td><?= rupiah($row->harga_laundry,'Rp. '); ?></td> 
            <td><?php 
            if($row->id_jenis_laundry=='1'){
              $jumlahx = round((int)$row->jumlah_laundry);
            }else{
              $jumlahx = $row->jumlah_laundry;
            }
            echo $jumlahx.' '.$row->satuan; ?></td>
            <td><?= rupiah($row->total,'Rp. '); $total += $row->total; ?></td> 
            <td><?php if(empty($row->keterangan)){echo "-";}else{echo $row->keterangan;} ; ?></td> 
            <td><?= $row->nama_lengkap; ?></td> 
        </tr>
    <?php }  ?>
    
  </tbody>
  <tfoot>
    <tr style="background-color: #cbffc9;">
      <td colspan="6"></td>
      <td align="right" style="font-weight: bold;"> Total</td>
      <td colspan="3" style="font-weight: bold;"> <?= rupiah($total,'Rp. '); ?></td>
    </tr>
  </tfoot>
</table>

<script type="text/javascript">
  $('.datatable').DataTable();

  $(document).ready(function(){
    $(".clickable_row").click(function(){
        $(".clickable_row").removeClass("selected");
        $(this).addClass("selected"); 
        tr_id = $(this).data("id");
        $(".clickable_row_button").removeClass("cent-hidden");
    });
  });
</script>