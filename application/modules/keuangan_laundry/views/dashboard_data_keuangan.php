<link rel="stylesheet" href="<?= base_url('assets/vendors/chartjs/dist/Chart.css'); ?>">
<script src="<?= base_url('assets/vendors/chartjs/dist/Chart.js'); ?>"></script> 
    
<div class="row">
     <div class="col-md-3">
        <div class="card text-white bg-success">
            <div class="card-body" style="text-align: center; ">
                <h4> Total Pemasukkan Laundry</h4>
                <h2 style="padding-top: 5px;"> <?= rupiah($total_pemasukkan_harian,"Rp. "); ?></h2>
            </div>
        </div>
    </div> 

    <div class="col-md-3">
        <div class="card text-white bg-info">
            <div class="card-body" style="text-align: center; ">
                <h4> Total Pemasukkan Lainya</h4>
                <h2 style="padding-top: 5px;"> <?= rupiah($total_pemasukkan_lainya,"Rp. "); ?></h2>
            </div>
        </div>
    </div> 

    <div class="col-md-3">
        <div class="card text-white bg-warning">
            <div class="card-body" style="text-align: center; ">
                <h4> Pengeluaran Laundry </h4>
                <h2 style="padding-top: 5px;"> <?= rupiah($total_pengeluaran_laundry,"Rp. "); ?></h2>
            </div>
        </div>
    </div> 

    <div class="col-md-3">
        <div class="card text-white bg-danger">
            <div class="card-body" style="text-align: center; ">
                <h4> Pengeluaran Keluarga </h4>
                <h2 style="padding-top: 5px;"> <?= rupiah($total_pengeluaran_keluarga,"Rp. "); ?></h2>
            </div>
        </div>
    </div> 

</div>

<br>  
    <form id="frmz">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row mb-2" >
                    <div class="col-md-12 cent-left mb-3"><h3 >Detail Keuangan Laundry</h3></div> 
                        <div class="col-md-5">  
                            <div class="form-group"> 
                                <label>Dari Tanggal</label>
                                <input class="form-control form-control-sm CentreeTgl centree-datetimepicker" autocomplete="off" name="dari" value="<?= $date->first_date; ?>" required>
                              </div>
                        </div> 
                        <div class="col-md-5" >  
                            <div class="form-group">
                                <label>Sampai Tanggal</label>
                                <input class="form-control form-control-sm CentreeTgl centree-datetimepicker" value="<?= $date->last_date; ?>" autocomplete="off" name="sampai"  required>
                              </div>
                        </div>
                        <div class="col-md-2">  
                            <div class="form-group"  style="padding-top: 20px;">
                                <center>
                                    <button type="button" class="btn btn-warning btn-lg" id="btn_reset"> <i class="fa fa-refresh"></i> Reset </button>
                                    <button class="btn btn-primary btn-lg" id="btn_filter"> <i class="fa fa-filter" type="submit"></i> Filter </button>
                                </center>
                              </div>
                        </div>    
                    </div>
                </div> 
                <div id="div-alert" class="alert alert-warning" style="display: none;"> </div>
                <div class="row" id="data-xyz"> 
                        
                </div> 
            </div>
          </div>
    </form>   
    <br>
 
  

    <div class="col-md-12">
        <div class="card">
            <div class="card-body"> 
            <div class="row" style="padding-bottom: 10px;">
                <div class="col-md-8 cent-left"><h3 class="">Graph Total Pemasukkan Laundry Bulanan</h3></div>
            </div>
               
                <div class="col-12">
                   <canvas id="myChart"  height="100"></canvas>
                </div> 
            </div>
          </div>
    </div> 
    <br>

    <div class="col-md-12">
        <div class="card">
            <div class="card-body"> 
            <div class="row" style="padding-bottom: 10px;">
                <div class="col-md-8 cent-left"><h3 class="">Graph Keuangan Laundry</h3></div> 
            </div>
                <div class="col-12">
                   <canvas id="myChart_keuangan"  height="100"></canvas>
                </div> 
            </div>
          </div>
    </div> 
    <br>
<script>
var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: JSON.parse('<?= $data_graphic_bulanan["label"]; ?>'),
        datasets: [{
            label: 'Total Pendapatan',
            data: JSON.parse('<?= $data_graphic_bulanan["data"]; ?>'),
            backgroundColor:  'rgba(52, 208, 235, 0.2)' ,
            borderColor: 'rgba(52, 208, 235, 1)' ,
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>


<script>
var ctx = document.getElementById('myChart_keuangan');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: JSON.parse('<?= $data_graphic_keuangan["label"]; ?>'),
        datasets: [{
            label: 'Total Pendapatan',
            data: JSON.parse('<?= $data_graphic_keuangan["data"]; ?>'),
            backgroundColor: [
                'rgba(252, 3, 107, 0.2)'
            ],
            borderColor: [
                'rgba(252, 3, 107, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>

<script type="text/javascript">
    var first_date = "<?= $date->first_date; ?>";
    var last_date  = "<?= $date->last_date; ?>";

    $('#frmz').submit(function(event) { 
        event.preventDefault(); 
        var values = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: "<?= base_url('keuangan_laundry/get_dashboard_chart'); ?>",
            data: values,
            beforeSend: function(){
              pesan_tunggu("Please Wait","div-alert",'div');
            },
            success: function (data) {
                reset_pesan_tunggu("div-alert","div"); 
                $("#data-xyz").empty();
                $("#data-xyz").append(data);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                pesan_error("Gagal!", errorThrown);
            }
        });
        return false; //stop
    }); 

    $('#btn_reset').click(function(event) {
        var values = "dari="+first_date+"&sampai="+last_date;
        $.ajax({
            type: 'POST',
            url: "<?= base_url('keuangan_laundry/get_dashboard_chart'); ?>",
            data: values,
            beforeSend: function(){
              pesan_tunggu("Please Wait","div-alert",'div');
            },
            success: function (data) {
                reset_pesan_tunggu("div-alert","div"); 
                $("#data-xyz").empty();
                $("#data-xyz").append(data);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                pesan_error("Gagal!", errorThrown);
            }
        });
        return false; //stop
    });
</script>

<script type="text/javascript">
    setTimeout(function() {
        $('#frmz').submit();
    }, 500);
</script>