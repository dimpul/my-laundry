<?php
	#inisialisasi
  $id         = @$data->id;
  $tanggal    = @$data->tanggal;
  $jumlah     = @$data->jumlah;
  $keterangan = @$data->keterangan; 
?>
<form id="global-form">
  <div class="form-group">
    <label>Tanggal</label>
    <input class="form-control CentreeTgl centree-datetimepicker" autocomplete="off" name="tanggal" value="<?= $tanggal; ?>" required>
  </div>
  <div class="form-group ">
    <label>Jumlah</label>
    <div class="input-group">
    <input class="form-control CentreeRupiah" name="jumlah" type="text" value="<?= $jumlah; ?>">
    </div>
  </div>

  </div>
  <div class="form-group"> 
    <label>Keterangan</label>
    <div class="input-group">
    <textarea class="form-control" name="keterangan"><?= $keterangan; ?></textarea>
    </div>
  </div>

  <?php
	if($type=="update"){
		$url = base_url('keuangan_laundry/update_pemasukkan_lainya_conf'); ?>
		 <div class="form-group cent-hidden">
		    <label>ID</label>
		    <input class="form-control" value="<?= $id; ?>" name="id" minlength="2" type="text" required>
		  </div>
	<?php }else{
		$url = base_url('keuangan_laundry/tambah_pemasukkan_lainya_conf');
	}

?>
  <center><input id="submit-btn" class="btn btn-primary" type="submit" value="Submit"></center>
</form>

<script type="text/javascript">
    $('.tgl').datetimepicker({
          format: 'HH:mm',
          useCurrent: true,
          widgetPositioning: {
            horizontal: 'left',
            vertical: 'top'
        }
      });
  </script>

<script type="text/javascript">
   $('#global-form').submit(function(event) { 
        event.preventDefault(); 
        var values = $(this).serialize();
        var returnx = simple_ajax(values,"<?= $url; ?>","","Berhasil!","Gagal!","submit-btn","button");
        if(returnx==1){
          get_table_pemasukkan();
        }
        return false; //stop
    });

   $(".CentreeTgl").CentreeTgl();
   $('.centree-datetimepicker').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent: false,
        sideBySide: true
    }).on('dp.change', function (e) { 
    //get attribut
        var name    = $(this).attr('name');
        var name_tmp  = name.split("___");
        var name_real = name_tmp[1];
        var value     = $(this).val();

        //change format date
        var arr = value.split("/");
        var datex = arr[2] + "-" + arr[1] + "-" + arr[0];

        //change date
        $("input[name='"+name_real+"']").val(datex); 

    }); 

   $(".CentreeRupiah").CentreeRupiah();
   $(".CentreeRupiah").keyup(function(){
            //get attribut
          var name    = $(this).attr('name');
          var name_tmp  = name.split("___");
          var name_real = name_tmp[1];
          var angka     = $(this).val();
          var prefix    = "Rp. ";

          //change rupiah
          returnx = CentreeFormatRupiah(angka);

          var value  = $(this).val(returnx[0]);
          $("input[name='"+name_real+"']").val(returnx[1]);  
      });
</script>