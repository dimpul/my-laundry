 <table class="table table table-hover datatable">
  <thead>
    <tr>
        <th>ID</th>
        <th>Tanggal</th>
        <th>Jumlah</th>
        <th>Keterangan</th>
        <th>Updated By</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($data as $row) { ?>
        <tr class="clickable_row" data-id="<?= $row->id; ?>" class="menu">
            <td><?= $row->id; ?></td>
            <td><?= $row->hari.", ".$row->tanggal; ?></td>
            <td><?= rupiah($row->jumlah,"Rp. "); ?></td>
            <td><?= $row->keterangan; ?></td> 
            <td><?= $row->nama_lengkap; ?></td> 
        </tr>
    <?php }  ?>
    
  </tbody>
</table>