<div class="col-md-4 col-xs-12">
     <canvas id="myChart_pengeluaran"  height="300" class="pb-3"></canvas>
     <div class="col-md-12">
        <div class="card text-white bg-info">
            <div class="card-body" style="text-align: center; ">
                <h4> Total Pemasukkan</h4>
                <h2 style="padding-top: 5px;"> <?= rupiah($data_total['debit'],"Rp. "); ?></h2>
            </div>
        </div>
    </div> 
    <div class="col-md-12">
        <div class="card text-white bg-danger">
            <div class="card-body" style="text-align: center; ">
                <h4> Total Pengeluaran</h4>
                <h2 style="padding-top: 5px;"> <?= rupiah($data_total['kredit'],"Rp. "); ?></h2>
            </div>
        </div>
    </div> 

  </div> 
  <div class="col-md-8 col-xs-12" style="padding-right: 20px;">
    <div class="table-responsive">
     <table class="table table-hover table-striped datatable">
      <thead>
      <tr align="center">
         <th>Tanggal</th>
         <th>Tipe</th>
         <th>Jumlah</th>
         <th>Keterangan</th>
         <th>Insert by</th> 
      </tr>
      </thead>
      <tbody>
         <?php
          foreach ($data as $row) { 
            if($row->jenis=='kredit'){
              $color = "#ffe6e6";
            }else{
              $color = "#cbffc9";
            }
            ?>
            <tr style="background-color: <?= $color; ?>">
               <td><?= $row->hari.", ".tgl_indo($row->tanggal); ?></td>
               <td><?= $row->tipe; ?></td>
               <td><?= rupiah($row->jumlah,"Rp. "); ?></td>
               <td><?= $row->keterangan; ?></td>
               <td><?= $row->updated_by; ?></td> 
            </tr>
          <?php } ?>
      </tbody>
     </table>
   </div>
  </div> 


<script>
var ctx = document.getElementById('myChart_pengeluaran');
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: JSON.parse('<?= $data_graphic["label"]; ?>'),
        datasets: [{
            label: 'Total Pengeluaran',
            data: JSON.parse('<?= $data_graphic["data"]; ?>'),
            backgroundColor: JSON.parse('<?= $data_graphic["bg"]; ?>'),
            borderColor: JSON.parse('<?= $data_graphic["line"]; ?>'),
            borderWidth: 1
        }]
    },
    options: {
        legend: {
            align: 'start',
            position: 'bottom'
        }
    }
});

$('.datatable').DataTable();
</script>

