<link rel="stylesheet" href="<?= base_url('assets/vendors/chartjs/dist/Chart.css'); ?>">
<script src="<?= base_url('assets/vendors/chartjs/dist/Chart.js'); ?>"></script> 
    
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
            <div id="div-alert" class="alert alert-warning" style="display: none;"> </div>
            <div class="row" style="padding-bottom: 10px;">
                <div class="col-md-8 cent-left"><h3 class="">Graph Total Pemasukkan Harian</h3></div> 
               <!--  <div class="col-md-4 cent-right" id="btn_config">
                   <select class="form-control ">
                     <option value="day"> Day </option>
                     <option value="month"> Month </option>
                   </select>
                </div> -->
            </div>
               
                <div class="col-12">
                   <canvas id="myChart"  height="100"></canvas>
                </div> 
            </div>
          </div>
    </div> 
    <br>

    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
            <div id="div-alert" class="alert alert-warning" style="display: none;"> </div>
            <div class="row" style="padding-bottom: 10px;">
                <div class="col-md-7 cent-left"><h3 class="">Pemasukkan Harian</h3></div>
                <div class="col-md-5 cent-right" id="btn_config">
                    <button class="btn btn-success btn-sm clickable_row_button" id="tambah"  data-toggle="tooltip" data-placement="bottom" title="Tambah"><i class="fa fa-plus"></i> Tambah </button>
                    <button class="btn btn-primary btn-sm cent-hidden clickable_row_button" id="update"  data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-eye"></i> Lihat Detail</button>
                    <button class="btn btn-danger btn-sm cent-hidden clickable_row_button" id="delete"  data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-close"></i> Hapus Data</button>
                </div>
            </div>
              
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table class="table table table-hover datatable">
                      <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Hari</th>
                            <th>Total Pemasukkan</th> 
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($data_pemasukkan as $row) { ?>
                            <tr class="clickable_row" data-id="<?php $date=date_create($row->tanggal); echo date_format($date,"d-m-Y"); ?>" class="menu">
                                <td><?= tgl_indo($row->tanggal); ?></td>
                                <td><?= $row->hari; ?></td>
                                <td><?= rupiah($row->total_pemasukkan,'Rp. '); ?></td> 
                            </tr>
                        <?php }  ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
    </div> 

<!-- MODAL GLOBAL -->
<div class="modal fade" id="modal_xyz" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Detail Pemasukkan Harian</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> 
      </div>
      <div class="modal-body" id="ModalBody"> zx</div>
    </div>
  </div> 
</div>
<!-- Modal Ends -->

<script type="text/javascript">
  $(document).ready(function(){
    $("#tambah").click(function(){
        window.location="<?= base_url('keuangan_laundry/tambah'); ?>";
    });


    $("#delete").click(function(){
           pesan_confirm("Apakah anda yakin?", "Menghapus Semua data pemasukkan tanggal: "+tr_id, "Ya, Hapus").then((result) => {
            if(result===true){
                simple_ajax('tanggal='+tr_id,"keuangan_laundry/delete_conf","","Berhasil!","Gagal!","div-alert","div");
            }
        });
      });

    $("#update").click(function(){
        get_append_ajax("tanggal="+tr_id, "<?= base_url('keuangan_laundry/get_list_pemasukkan_harian'); ?>", "ModalBody","modal_xyz","modal");
        $("#modal_xyz").modal('show');
      });

  });
</script>

<script>
var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: JSON.parse('<?= $data_graphic["label"]; ?>'),
        datasets: [{
            label: 'Total Pendapatan',
            data: JSON.parse('<?= $data_graphic["data"]; ?>'),
            backgroundColor: [
                'rgba(52, 208, 235, 0.2)'
            ],
            borderColor: [
                'rgba(52, 208, 235, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>