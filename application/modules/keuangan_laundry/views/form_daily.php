<?php 
  $tanggal = @$data->tanggal;
  $id = @$data->id; 
  $nama_cust = @$data->nama_customer; 
  $keterangan = @$data->keterangan; 
?>
<form id="global-form">
  <div class="form-group">
    <label>Tanggal</label>
    <input class="form-control CentreeTgl centree-datetimepicker" autocomplete="off" name="tgl"  value="<?= $tanggal; ?>" required>
  </div>
  <div class="form-group ">
    <label>Nama Customer</label>
    <div class="input-group">
    <input class="form-control CentreeRupiah" value="<?= $nama_cust; ?>" name="name[1]" type="text" value="" required>
    </div>
  </div>
  <div class="form-group ">
    <label>Jenis Laundry</label>
    <div class="input-group" id="jl__1"> </div>
  </div>
  <div class="form-group ">
    <label>Ukuran</label>
    <div class="input-group" id="ukuran__1"> </div>
  </div>
  <div class="form-group ">
    <label>Jumlah</label>
    <div class="input-group" id="jumlah__1"> </div>
  </div>
  <div class="form-group ">
    <label>Harga</label>
    <div class="input-group" id="harga__1"> </div>
  </div>

  <div class="form-group ">
    <label>Total</label>
    <div class="input-group" id="total__1">  </div>
  </div>

  <div class="form-group ">
    <label>Keterangan</label>
    <textarea name="keterangan[1]" class="form-control"><?= $keterangan; ?></textarea>
  </div>
  </div> 

  <?php
	if($type=="update"){
		$url = base_url('keuangan_laundry/update_conf/satuan');   ?>
		 <div class="form-group cent-hidden">
		    <label>ID</label>
		    <input class="form-control" value="<?= $id; ?>" name="id" minlength="2" type="text" required>
		  </div>
	<?php }else{
		$url = base_url('keuangan_laundry/tambah_conf');
	}

?>
  <center><input id="submit-btn" class="btn btn-primary" type="submit" value="Submit"></center>
</form>

<script type="text/javascript">
    $('.tgl').datetimepicker({
          format: 'HH:mm',
          useCurrent: true,
          widgetPositioning: {
            horizontal: 'left',
            vertical: 'top'
        }
      });
  </script>

<script type="text/javascript">
   $('#global-form').submit(function(event) { 
        event.preventDefault(); 
        var values = $(this).serialize();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "<?= $url; ?>",
            data: values,
            beforeSend: function(){
              pesan_tunggu("Please Wait","submit-btn",'button');
            },
            success: function (data) {
                reset_pesan_tunggu("submit-btn","button"); 
                if(data.stt=1){
                  pesan_berhasil_swal("Berhasil",  data.message, url = "#");
                }else{
                  pesan_error("Gagal!", data.message);
                }
                $("#GlobalModal").modal('hide');
                get_data();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                pesan_error("Gagal!", errorThrown);
            }
        });
        return false; //stop
    });



   $(".CentreeTgl").CentreeTgl();
   $('.centree-datetimepicker').datetimepicker({
        format: 'DD/MM/YYYY',
        useCurrent: false,
        sideBySide: true
    }).on('dp.change', function (e) { 
    //get attribut
        var name    = $(this).attr('name');
        var name_tmp  = name.split("___");
        var name_real = name_tmp[1];
        var value     = $(this).val();

        //change format date
        var arr = value.split("/");
        var datex = arr[2] + "-" + arr[1] + "-" + arr[0];

        //change date
        $("input[name='"+name_real+"']").val(datex); 

    }); 
 </script>


<script type="text/javascript">
  //inisialisassi--------------------------------------------------------------------
  var data_jenis_laundry = JSON.parse('<?= $jenis_laundry; ?>');
  var data_harga    = JSON.parse('<?= $harga; ?>');
  var i = 1; 

  //get droplist jenis laundry-------------------------------------------------------
  function get_jl_droplist(row_num,datax=""){
  var select_option = "";
    if(datax !== ""){
      select_option = "";
      $.each(data_jenis_laundry, function( key, value ) {
         var selectedx = "";
         var valx = value['id']+'___'+value['data_ukuran']+'___'+value['satuan'];
         if(value['id']==datax.id_jenis_laundry){
            selectedx = "selected='selected'";
            setTimeout(function() { get_ukuran_droplist(row_num,valx,datax);}, 200);
         }
         select_option += '<option '+selectedx+' value="'+valx+'">'+value['jenis_laundry']+'</option>';
      });
    }else{
      $.each( data_jenis_laundry, function( key, value ) {
       select_option += '<option value="'+value['id']+'___'+value['data_ukuran']+'___'+value['satuan']+'">'+value['jenis_laundry']+'</option>';
      });
    }
    var select = '<select onchange="get_ukuran_droplist('+row_num+',this.value)" class="form-control form-control-sm select2" name="jenis_laundry['+row_num+']" required>';
    select += '<option value=""> -- Pilih -- </option>';
    select += select_option;
    select += '</select>';
    return select;
  }
 

  function get_ukuran_droplist(row_num,data,datay=""){
    //kosongkan
    $("#jumlah__"+row_num).empty();
    $("#total__"+row_num).empty();
    $("#harga__"+row_num).empty();

    //split data
    var datax = data.split("___");
    var jenis_laundry = datax[0];
    var data_ukuran   = datax[1];
    var satuan      = datax[2];
    estes(datax);

    //get droplist ukuran
    var select = '<select onchange="get_jumlah_form('+row_num+",'"+satuan+"'"+',this.value)" class="form-control form-control-sm select2" name="ukuran['+row_num+']" required>';
    select += '<option value=""> -- Pilih -- </option>';
    $.each( data_harga, function( key, value ) { 
      var harga = value['harga'];
      if(value['id_jenis_laundry']==jenis_laundry){
        var selectedx = "";
          var valx = value['id']+'___'+value['harga'];
          if(value['id']==datay.id_ukuran){
            selectedx = "selected='selected'";
            //setTimeout(function() { get_ukuran_droplist(row_num,valx,datax);}, 200);
            get_jumlah_form(row_num,satuan,valx,datay);
          } 

        if(data_ukuran=='1'){
          select += '<option '+selectedx+' value="'+valx+'">'+value['ukuran']+'</option>';
        }else{
          select = '<input type="text" value="'+value['id']+'___'+value['harga']+'" name="ukuran['+row_num+']" readonly hidden /> <center> - </center>';
          get_jumlah_form(row_num,satuan,valx,datay);
        }
      } 
    });
    select += '</select>';
    $("#ukuran__"+row_num).empty();
    $("#ukuran__"+row_num).append(select);
    hitung_total_all();
  }

  function get_jumlah_form(row_num,satuan,data,datay=""){
    //split data
    var datax = data.split("___");
    var jenis_laundry = datax[0];
    var harga   = datax[1];
    //estes(datax);

    var jumlahx = 0;
    var totalx = 0;
    if(datay !== ""){
      jumlahx = datay.jumlah_laundry;
      totalx = datay.total;
    }

    var input = '<input class="form-control form-control-sm" value="'+jumlahx+'"   onkeyup="hitungtotal('+row_num+')" onchange="hitungtotal('+row_num+')" type="text" name="jumlah['+row_num+']" required>';
    $("#jumlah__"+row_num).empty();
    $("#jumlah__"+row_num).append(input);

    var input2 = '<input id="total_value__'+row_num+'" value="'+totalx+'"  class="form-control form-control-sm CentreeRupiah" type="text" name="total['+row_num+']" readonly style="background-color: #e0ffe4;" onkeyup="hitungtotal('+row_num+')" onchange="hitungtotal('+row_num+')">';
    $("#total__"+row_num).empty();
    $("#total__"+row_num).append(input2);
    $("#total_value__"+row_num).CentreeRupiah();

    var input2 = '<input id="harga_value__'+row_num+'" class="form-control form-control-sm CentreeRupiah" type="text" name="harga['+row_num+']" value="'+harga+'" required onkeyup="hitungtotal('+row_num+')" onchange="hitungtotal('+row_num+')">';
    $("#harga__"+row_num).empty();
    $("#harga__"+row_num).append(input2);
    $("#harga_value__"+row_num).CentreeRupiah();

  }

  function hitungtotal(row_num){ 
    var harga_new = $("input[name='harga["+row_num+"]']").val();
    var jumlah    = $("input[name='jumlah["+row_num+"]']").val();
    //estes(harga_new);
    var total = Math.round(parseInt(harga_new)*jumlah); 

        //change rupiah
        returnx = CentreeFormatRupiah(total.toString(),"Rp. ","yes");
    $("input[name='total["+row_num+"]']").val(total);
    $("input[name='CentreeRupiah___total["+row_num+"]']").val(returnx);
    hitung_total_all();

    $(".CentreeRupiah").keyup(function(){
            //get attribut
          var name    = $(this).attr('name');
          var name_tmp  = name.split("___");
          var name_real = name_tmp[1];
          var angka     = $(this).val();
          var prefix    = "Rp. ";

          //change rupiah
          returnx = CentreeFormatRupiah(angka);

          var value  = $(this).val(returnx[0]);
          $("input[name='"+name_real+"']").val(returnx[1]); 
          hitung_total_all();
      });
  }

  function hitung_total_all(){
    var total = 0; 
    $('input[name*="total"]').each(function(){
      var harga = this.value;
      if(isNaN(harga) || harga==""){
        
      }else{ 
        total = parseInt(total)+ parseInt(harga);
      }
      
    });
 
    //estes(total);
    var total_rupiah = CentreeFormatRupiah(total.toString(),"Rp. ","yes");
    $("#total_column").empty();
    $("#total_column").append(total_rupiah);

  }

  function add_row(data=""){
    var nama = "";
    if(data!==""){
      nama = data.nama_customer;
    }
    var table = $("#tbl");

    //row
    var append = get_jl_droplist(i,data);
    $("#jl__1").append(append);  
  }
 
  $('#formxyz').submit(function(event) { 
        event.preventDefault(); 
        var values = $(this).serialize();
        simple_ajax(values,"<?= $url; ?>","<?= base_url('keuangan_laundry'); ?>","Berhasil!","Gagal!","div-alert","div");
        return false;
    });
  
  $(".CentreeTgl").CentreeTgl();
</script>

<?php  //estes($data); 
if(!empty($data)){
  $tmp = json_encode($data);
}else{
  $tmp = "[]";
}
    
?>
    <script type="text/javascript"> 
      var json_data = JSON.parse('<?= $tmp; ?>');
      add_row(json_data); 
    </script> 