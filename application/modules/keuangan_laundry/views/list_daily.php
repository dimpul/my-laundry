  <div class="row">
    <div class="col-md-12 col-xs-12">
      <div class="card">
            <div class="card-body"> 
            <div class="row" style="padding-bottom: 10px;">
                <div class="col-md-12 cent-left"><h3 class="">Filter Data</h3></div> 
            </div>
              <form id="frmz">
              <div class="row">
                <div class="col-md-12">  
                    <div class="form-group" style="margin-bottom: 5px;"> 
                        <label>Tanggal</label>
                        <input class="form-control form-control-sm CentreeTgl centree-datetimepicker" autocomplete="off" name="tanggal" value="<?= date('Y-m-d'); ?>" onkeydown="return false;">
                      </div>
                      <center> <button class="btn btn-success"> <i class="fa fa-filter"></i> Filter </button></center>
                </div> 
              </div>
              </form>
            </div>
          </div>
    </div>
    <div class="col-md-12 col-xs-12"> 
        <div class="card">
            <div class="card-body">
            <div id="div-alert" class="alert alert-warning" style="display: none;"> </div>
            <div class="row" style="padding-bottom: 10px;">
                <div class="col-md-7 cent-left"><h3 class="">Data Pemasukkan Harian</h3></div>
                <div class="col-md-5 cent-right" id="btn_config"> 
                    <button class="btn btn-success btn-sm" id="tambah" data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-plus"></i> Tambah Data</button>
                    <button class="btn btn-primary btn-sm cent-hidden clickable_row_button" id="update"  data-toggle="tooltip" data-placement="bottom" title="Edit Data"><i class="fa fa-pencil"></i> Update Data</button>
                    <button class="btn btn-danger btn-sm cent-hidden clickable_row_button" id="delete"  data-toggle="tooltip" data-placement="bottom" title="Hapus Data"><i class="fa fa-close"></i> Hapus Data</button>
                </div>
            </div>
              
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive" id="data-xyz">
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
    </div> 
  </div>


<script type="text/javascript">

  $('#frmz').submit(function(event) { 
        event.preventDefault(); 
        var values = $(this).serialize(); 
        get_data(values)
        return false; //stop
    }); 

  function get_data(values=""){
      $.ajax({
          type: 'POST',
          url: "<?= base_url('keuangan_laundry/get_daily_table'); ?>",
          data: values,
          beforeSend: function(){
            pesan_tunggu("Please Wait","div-alert",'div');
          },
          success: function (data) {
              reset_pesan_tunggu("div-alert","div"); 
              $("#data-xyz").empty();
              $("#data-xyz").append(data);
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
              pesan_error("Gagal!", errorThrown);
          }
      });
  }

  $(document).ready(function(){ 

    $("#tambah").click(function(){
        get_append_ajax("type=tambah", "<?= base_url('keuangan_laundry/get_form_daily'); ?>", "","div-alert","div", "yes" , "<i class='fa fa-plus'></i> Tambah Pemasukkan Lainya");
    });


    $("#delete").click(function(){
           pesan_confirm("Apakah anda yakin?", "Menghapus Data Pemasukkan #"+tr_id, "Ya, Hapus").then((result) => {
            if(result===true){
                simple_ajax('id='+tr_id,"<?= base_url('keuangan_laundry/delete_conf_satuan'); ?>","","Berhasil!","Gagal!","div-alert","div");
            }
        });
      });

    $("#update").click(function(){
        get_append_ajax("type=update&id="+tr_id, "<?= base_url('keuangan_laundry/get_form_daily'); ?>", "","div-alert","div", "yes" , "<i class='fa fa-pencil'></i> Edit Data Pemasukkan Lainya") 
      });

  });
</script>
 
 <script type="text/javascript">
    setTimeout(function() {
        $('#frmz').submit();
    }, 500);
</script> 