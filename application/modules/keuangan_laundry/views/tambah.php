
<form id="formxyz">
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
            	<div id="div-alert" class="alert alert-warning" style="display: none;"> </div>
	            <div class="row" style="padding-bottom: 10px;">
	                <div class="col-md-12 cent-left mb-3"><h3> <i class="fa fa-plus"></i> Tambah Pemasukkan Harian</h3></div>
	                <div class="col-md-12"> 
						    <label>Tanggal</label>
						    <input type="text" class="form-control form-control-sm centree-datetimepicker CentreeTgl" autocomplete="off" name="tgl" required="">  
	                </div>

	                
	            </div>
            </div>
        </div>
    </div>
</div>

<br>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body table-responsive">
            	<table class="table table-striped table-sm " >
            		<tr >
            			<th style="width: 5%; padding: 20px;"></th>
            			<th style="width: 15%;">Nama Customer</th>
            			<th style="width: 15%;">Jenis Laundry</th>
            			<th style="width: 15%;">Ukuran</th>
            			<th style="width: 10%;">Jumlah</th>
            			<th style="width: 10%;">Harga</th>
            			<th style="width: 20%;">Total</th>
            			<th style="width: 20%;">Keterangan</th>
            		</tr>
            		<tbody id="tbl"></tbody>
            		<tfoot >
            			<tr style=" background-color: #caffc2; ">
            			<td colspan="4"> <br> <br> &nbsp</td>
            			<td colspan="2" style=" font-size: 15pt; font-weight: bold; text-align: right;">
            				Total : 
            			</td>
            			<td colspan="2" style="font-size: 15pt; font-weight: bold; " id="total_column" >
            			</td>
            			</tr>
            		</tfoot>
            	</table>

            	<div class="col-md-12 pt-2 text-center" id="btn_config" > 
            		<button onclick="history.back()" class="btn btn-secondary" type="button" title="Kembali"><i class="fa fa-arrow-left"></i> Kembali </button> 
                    <button onclick="add_row()" class="btn btn-success" id="tambah" type="button" title="Tambah Baris"><i class="fa fa-plus"></i> Baris Baru</button> 
                    <button class="btn btn-primary" title="Simpan"> <i class="fa fa-save"></i> Simpan </button>
                </div>
            </div>
        </div>
    </div>
</div> 
</form>


<script type="text/javascript">
	//inisialisassi--------------------------------------------------------------------
	var i = 1;
	var data_jenis_laundry = JSON.parse('<?= $jenis_laundry; ?>');
	var data_harga 	  = JSON.parse('<?= $harga; ?>');
	var select_option = "";
	$.each( data_jenis_laundry, function( key, value ) {
	   select_option += '<option value="'+value['id']+'___'+value['data_ukuran']+'___'+value['satuan']+'">'+value['jenis_laundry']+'</option>';
	});

	//get droplist jenis laundry-------------------------------------------------------
	function get_jl_droplist(row_num){
		var select = '<select onchange="get_ukuran_droplist('+row_num+',this.value)" class="form-control form-control-sm select2" name="jenis_laundry['+row_num+']" required>';
		select += '<option value=""> -- Pilih -- </option>';
		select += select_option;
		select += '</select>';
		return select;
	}
 

	function get_ukuran_droplist(row_num,data){
		//kosongkan
		$("#jumlah__"+row_num).empty();
		$("#total__"+row_num).empty();
		$("#harga__"+row_num).empty();

		//split data
		var datax = data.split("___");
		var jenis_laundry = datax[0];
		var data_ukuran   = datax[1];
		var satuan  	  = datax[2];
		//estes(datax);

		//get droplist ukuran
		var select = '<select onchange="get_jumlah_form('+row_num+",'"+satuan+"'"+',this.value)" class="form-control form-control-sm select2" name="ukuran['+row_num+']" required>';
		select += '<option value=""> -- Pilih -- </option>';
		$.each( data_harga, function( key, value ) { 
			var harga = value['harga'];
			if(value['id_jenis_laundry']==jenis_laundry){
				if(data_ukuran=='1'){
					select += '<option value="'+value['id']+'___'+value['harga']+'">'+value['ukuran']+'</option>';
				}else{
					select = '<input type="text" value="'+value['id']+'___'+value['harga']+'" name="ukuran['+row_num+']" readonly hidden /> <center> - </center>';
					get_jumlah_form(row_num,satuan,value['id']+'___'+value['harga']);
				}
			} 
		});
		select += '</select>';
		$("#ukuran__"+row_num).empty();
		$("#ukuran__"+row_num).append(select);
		$('.select2').select2({
		    theme: "bootstrap"
		});
		hitung_total_all();
	}

	function get_jumlah_form(row_num,satuan,data){
		//split data
		var datax = data.split("___");
		var jenis_laundry = datax[0];
		var harga   = datax[1];
		//estes(datax);

		var input = '<input class="form-control form-control-sm"   onkeyup="hitungtotal('+row_num+')" onchange="hitungtotal('+row_num+')" type="text" name="jumlah['+row_num+']" required>';
		//input += '<small class="text-success">Harga: '+CentreeFormatRupiah(harga,"Rp. ","yes")+'/'+satuan+'</small>';
		$("#jumlah__"+row_num).empty();
		$("#jumlah__"+row_num).append(input);

		var input2 = '<input id="total_value__'+row_num+'" class="form-control form-control-sm CentreeRupiah" type="text" name="total['+row_num+']" readonly style="background-color: #e0ffe4;" onkeyup="hitungtotal('+row_num+')" onchange="hitungtotal('+row_num+')">';
		$("#total__"+row_num).empty();
		$("#total__"+row_num).append(input2);
		$("#total_value__"+row_num).CentreeRupiah();

		var input2 = '<input id="harga_value__'+row_num+'" class="form-control form-control-sm CentreeRupiah" type="text" name="harga['+row_num+']" value="'+harga+'" required  onchange="hitungtotal('+row_num+')">';
		$("#harga__"+row_num).empty();
		$("#harga__"+row_num).append(input2);
		$("#harga_value__"+row_num).CentreeRupiah();

	}

	function hitungtotal(row_num){ 
		var harga_new = $("input[name='harga["+row_num+"]']").val();
		var jumlah    = $("input[name='jumlah["+row_num+"]']").val();
		//estes(harga_new);
		var total = Math.round(parseInt(harga_new)*jumlah); 

        //change rupiah
        returnx = CentreeFormatRupiah(total.toString(),"Rp. ","yes");
		$("input[name='total["+row_num+"]']").val(total);
		$("input[name='CentreeRupiah___total["+row_num+"]']").val(returnx);
		hitung_total_all();

		$(".CentreeRupiah").keyup(function(){
	          //get attribut
	        var name    = $(this).attr('name');
	        var name_tmp  = name.split("___");
	        var name_real = name_tmp[1];
	        var angka     = $(this).val();
	        var prefix    = "Rp. ";

	        //change rupiah
	        returnx = CentreeFormatRupiah(angka);

	        var value  = $(this).val(returnx[0]);
	        $("input[name='"+name_real+"']").val(returnx[1]); 
	        hitung_total_all();
	    });
	}

	function hitung_total_all(){
		var total = 0; 
		$('input[name*="total"]').each(function(){
			var harga = this.value;
			if(isNaN(harga) || harga==""){
				
			}else{ 
				total = parseInt(total)+ parseInt(harga);
			}
			
		});
 
		//estes(total);
		var total_rupiah = CentreeFormatRupiah(total.toString(),"Rp. ","yes");
		$("#total_column").empty();
		$("#total_column").append(total_rupiah);

	}

	function add_row(){
		var table = $("#tbl");

		//row
		var row = "<tr id='btn__"+i+"'>";
		row += "<td> <button onclick='remove_row("+i+")' class='btn btn-danger'><i class='fa fa-minus'></i> </td>";
		row += '<td> <input type="text" class="form-control form-control-sm" name="name['+i+']" required> </td>'; 
		row += '<td> '+get_jl_droplist(i)+'</td>'; 
		row += '<td id="ukuran__'+i+'">  </td>'; 
		row += '<td id="jumlah__'+i+'">  </td>'; 
		row += '<td id="harga__'+i+'">  </td>'; 
		row += '<td id="total__'+i+'">  </td>'; 
		row += '<td id="keterangan__'+i+'"> <textarea type="text" class="form-control form-control-sm" name="keterangan['+i+']"></textarea> </td>'; 
		row += "</tr>";

		//append
		table.append(row);

		i++;
		$('.select2').select2({
		    theme: "bootstrap"
		});
	}

	function remove_row(i){
		$("#btn__"+i).remove();
		hitung_total_all();
	}

	$(".btn-remove").on('click', function(e) {
		console.log('asd');	
	    var whichtr = $(this).closest("tr"); 
	    whichtr.remove();      
	});

	$('#formxyz').submit(function(event) { 
        event.preventDefault(); 
        var values = $(this).serialize();
        simple_ajax(values,"<?= base_url('keuangan_laundry/tambah_conf'); ?>","<?= ''; //base_url('keuangan_laundry/pemasukkan_harian'); ?>","Berhasil!","Gagal!","div-alert","div");
        return false;
    });
	
	$(".CentreeTgl").CentreeTgl();

	$('.centree-datetimepicker').datetimepicker({
	      format: 'DD/MM/YYYY',
	      useCurrent: false,
	      sideBySide: true
	  }).on('dp.change', function (e) { 
	  //get attribut
	      var name    = $(this).attr('name');
	      var name_tmp  = name.split("___");
	      var name_real = name_tmp[1];
	      var value     = $(this).val();

	      //change format date
	      var arr = value.split("/");
	      var datex = arr[2] + "-" + arr[1] + "-" + arr[0];

	      //change date
	      $("input[name='"+name_real+"']").val(datex); 

	  }); 
</script>

