<div class="row">
    <div class="col-12">
      <div class="table-responsive"> 
        <table class="table table-hover table-striped datatable">
          <thead>
            <tr style=" font-weight: bold !important;">
                <th style="width: 10%;">No</th>
                <th>Tanggal</th>
                <th>Nama</th>
                <th>Jenis Laundry</th>
                <th>Ukuran</th> 
                <th>Harga Laundry</th> 
                <th>Jumlah Laundry</th> 
                <th style="padding-right: 100px;">Total</th>  
                <th>Keterangan</th>  
                <th>Input By</th>
            </tr>
          </thead>
          <tbody>
            <?php $i=1; $total = 0; foreach ($data_pemasukkan as $row) { ?>
                <tr class="clickable_row" data-id="<?php $date=date_create($row->tanggal); echo date_format($date,"d-m-Y"); ?>" class="menu">
                    <td><?= $i; ?></td>
                    <td><?= tgl_indo($row->tanggal); ?></td>
                    <td><?= $row->nama_customer; ?></td>
                    <td><?= $row->jenis_laundry; ?></td>
                    <td>
                      <?php if($row->has_ukuran==1){echo $row->ukuran; }else{ echo "-"; } ?>
                    </td>
                    <td><?= rupiah($row->harga_laundry,"Rp. "); ?></td> 
                    <td><?php  
                        if($row->satuan=='Kg'){
                          echo $row->jumlah_laundry.' '.$row->satuan ;
                        }else{
                          echo round($row->jumlah_laundry).' '.$row->satuan ;
                        }
                     ?></td> 
                    <td><?= rupiah($row->total,"Rp. "); $total += $row->total;  ?></td> 
                    <td><?= $row->keterangan; ?></td>
                    <td><?= $row->nama_lengkap; ?></td>
                </tr>
            <?php $i++; }  ?>
            
          </tbody>
          <tfoot>
            <tr style="background-color: #d6ffdc;">
              <td colspan="6"></td>
              <td style="text-align: right; font-weight: bold;"><h4> Total : </h4></td>
              <td colspan="3" style=" font-weight: bold;"><h4>  <?= rupiah($total, "Rp. "); ?></h4></td>
            </tr>
          </tfoot>
        </table>

        <center><button onclick="window.location = '<?= base_url("keuangan_laundry/update/").$tgl; ?>'" class="btn btn-primary btn-lg" style="padding: 10px !important;"> <i class="fa fa-pencil"></i> Edit</button></center>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    $('.datatable').DataTable();
  </script>