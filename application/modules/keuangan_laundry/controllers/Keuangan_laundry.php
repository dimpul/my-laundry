<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keuangan_laundry extends CI_Controller {

	public function __construct()
    {
	    parent::__construct();
      $this->load->library('password');
      $this->load->model('m_keuangan_laundry');
    }


    public function index()
    {
      #Bagian Inisialisasi  
      #Bagian Proses
      $data_pemasukkan = h_crud_get_data('pemasukkan_harian_view',array(),'tanggal');

      //untuk graphic max 30 hari kebelakang
      $data_graphic_bulanan = $this->m_keuangan_laundry->data_graphic_bulanan();
      $data_graphic_pengeluaran = $this->m_keuangan_laundry->get_graphic_pengeluaran();
      $data_graphic_keuangan = $this->m_keuangan_laundry->get_graphic_keuangan_laundry();

      #Bagian Return 
      $data['total_pemasukkan_harian']   = $this->m_keuangan_laundry->get_total_keuangan_laundry('pemasukkan_harian_tb'); 
      $data['total_pemasukkan_lainya']   = $this->m_keuangan_laundry->get_total_keuangan_laundry('pemasukkan_lainya_tb');
      $data['total_pengeluaran_laundry'] = $this->m_keuangan_laundry->get_total_keuangan_laundry('pengeluaran_tb');
      $data['total_pengeluaran_keluarga'] = $this->m_keuangan_laundry->get_total_keuangan_laundry('pengeluaran_family_tb');
      $data['date']                       = $this->m_keuangan_laundry->get_first_last_date();
      $data['data_graphic_bulanan']         = $data_graphic_bulanan; 
      $data['data_graphic_pengeluaran']    = $data_graphic_pengeluaran;
      $data['data_graphic_keuangan']    = $data_graphic_keuangan;  
      load_view('dashboard_data_keuangan',$data,'Data Keuangan');
    }
 
    public function get_dashboard_chart(){
        #Bagian Inisialisasi 
        check_ajax_request();
        $tgl1 = post('dari');
        $tgl2 = post('sampai');

        #Bagian Proses
        $data_graphic = $this->m_keuangan_laundry->get_graphic_detail($tgl1, $tgl2);
        $total = $this->m_keuangan_laundry->get_total_detail($tgl1, $tgl2); 
        $data_keuangan  = $this->m_keuangan_laundry->get_data_detail($tgl1, $tgl2);   

        #Bagian Return
        $data['data_graphic'] = $data_graphic;
        $data['data'] = $data_keuangan;
        $data['data_total']   = $total;
        load_empty_view('dashboard_data_keuangan_chart',$data);
    }

    public function get_form_tambah(){
      load_empty_view('form_tambah');
    }

    public function get_list_pemasukkan_harian(){
        #Bagian Inisialisasi  
        $tgl = post('tanggal');
        $date  =date_create($tgl); 
        $date_ = date_format($date,"Y-m-d");

        #Bagian Proses
        $where = array('tanggal'=>$date_);
        $data_pemasukkan = h_crud_get_data('pemasukkan_harian_detail_view',$where,'id'); 

        #Bagian Return 
        $data['data_pemasukkan'] = $data_pemasukkan; 
        $data['tgl'] = $date_;
        load_empty_view('list_pemasukkan_harian_modal',$data);
    }

    public function pemasukkan_harian(){  
      #Bagian Inisialisasi  
      #Bagian Proses
      $data_pemasukkan = h_crud_get_data('pemasukkan_harian_view',array(),'tanggal');

      //untuk graphic max 30 hari kebelakang
      $data_graphic = $this->m_keuangan_laundry->get_graphic_pemasukkan_harian();

      #Bagian Return 
      $data['data_graphic']    = $data_graphic; 
      $data['data_pemasukkan'] = $data_pemasukkan; 
      load_view('list_pemasukkan_harian',$data,'Data Pemasukkan Harian');
    }

    public function daily(){
      //pemasukkan harian tetapi bisa diakses oleh semua karyawan

      #Bagian Inisialisasi  
      #Bagian Proses
      $data_pemasukkan = h_crud_get_data('pemasukkan_harian_view',array(),'tanggal');
 

      #Bagian Return  
      $data['data_pemasukkan'] = $data_pemasukkan; 
      load_view('list_daily',$data,'Data Pemasukkan Harian');
    }

    public function get_daily_table(){
      //pemasukkan harian tetapi bisa diakses oleh semua karyawan
      #Bagian Inisialisasi  
        check_ajax_request();
        $tgl = post('tanggal');
        if(empty($tgl)){
          $tgl = date('Y-m-d');
        }

      #Bagian Proses
        $data_pemasukkan = h_crud_get_data('pemasukkan_harian_detail_view',array('tanggal'=>$tgl),'id');
 
      #Bagian Return  
        $data['data_pemasukkan'] = $data_pemasukkan; 
        load_empty_view('list_daily_table',$data);
    }

    public function get_form_daily()
    {  
      #Bagian Inisialisasi
         $type = post('type');
         $id   = post('id');

      #Bagian Proses
         if($type=="update"){
            $data_checking_empty = array($id);
            checking_empty($data_checking_empty);
            $where      = array("id"=>$id);
            $data_pemasukkan  = h_crud_get_1_data("pemasukkan_harian_tb",$where);

            if(empty($data_pemasukkan)){
               echo "<center>Data Tidak Ditemukan</center>";
               return 0;
            }
         }else{
            $data_pemasukkan  = array();
         }
        $data_jenis_laundry = h_crud_get_data('jenis_laundry_tb',array(),'(CASE WHEN id=1 THEN 0 ELSE 1 END),(CASE WHEN id=2 THEN 0 ELSE 1 END), jenis_laundry');
        $data_harga         = h_crud_get_data('harga_tb',array(),'ukuran');

        #Bagian Return 
        $data['jenis_laundry'] = json_encode($data_jenis_laundry);
        $data['harga']         = json_encode($data_harga);
         $data['data'] = $data_pemasukkan;
         $data['type'] = $type;
         load_empty_view('form_daily',$data);
    }


    public function tambah(){  
      #Bagian Inisialisasi  
      #Bagian Proses
      $data_jenis_laundry = h_crud_get_data('jenis_laundry_tb',array(),'(CASE WHEN id=1 THEN 0 ELSE 1 END),(CASE WHEN id=2 THEN 0 ELSE 1 END), jenis_laundry');
      $data_harga         = h_crud_get_data('harga_tb',array(),'ukuran');

      #Bagian Return 
      $data['jenis_laundry'] = json_encode($data_jenis_laundry);
      $data['harga']         = json_encode($data_harga);
      load_view('tambah',$data,'Tambah Pemasukkan Harian','false');
    }

    public function update($tgl=""){  
      #Bagian Inisialisasi  
      $text = "Data Tidak Ditemukan! ";
      if(empty($tgl)){
        set_flashsession($text);
        redirect('keuangan_laundry/pemasukkan_harian');
      }
  
      $where          = array("tanggal"=>$tgl);
      $data_keuangan  = h_crud_get_data("pemasukkan_harian_tb",$where);
      if(empty($data_keuangan)){
        set_flashsession($text);
        redirect('keuangan_laundry/pemasukkan_harian');
      }

      #Bagian Proses
      $data_jenis_laundry = h_crud_get_data('jenis_laundry_tb',array(),'(CASE WHEN id=1 THEN 0 ELSE 1 END),(CASE WHEN id=2 THEN 0 ELSE 1 END), jenis_laundry');
      $data_harga         = h_crud_get_data('harga_tb',array(),'ukuran');

      #Bagian Return 
      $data['jenis_laundry'] = json_encode($data_jenis_laundry);
      $data['harga']         = json_encode($data_harga);
      $data['tgl']           = $tgl;
      $data['data']          = $data_keuangan;
      load_view('update',$data,'Edit Pemasukkan Harian Tanggal '.tgl_indo($tgl),'false');
    }

    public function tambah_conf(){  
      #Bagian Inisialisasi  
        check_ajax_request();
        $this->db->trans_start(); 
        $data_pemasukkan_laundry = array();
        $total   = post('total');
        $harga   = post('harga');
        $keterangan   = post('keterangan');
        $jumlah   = post('jumlah');
        $tanggal = post('tgl');
        $nama    = post('name');
        $jenis_laundry = post('jenis_laundry');
        $ukuran  = post('ukuran');
        $ukuran  = post('ukuran');
        

      #Bagian Proses  
        foreach ($total as $key => $row) {
           //jenis laundry
           $jl = explode("___", $jenis_laundry[$key]);

           //ukuran
           $u = explode("___", $ukuran[$key]);

           $tmp = array(
              'tanggal'=>$tanggal,
              'nama_customer'=>$nama[$key],
              'id_jenis_laundry'=>$jl[0],
              'id_ukuran'=>$u[0],
              'keterangan'=>$keterangan[$key],
              'harga_laundry'=>$harga[$key],
              'jumlah_laundry'=>$jumlah[$key],
              'total'=>$total[$key],
              'updated_by'=>get_session('id_user')
           );
           array_push($data_pemasukkan_laundry, $tmp);
        }
        $execute = h_crud_tambah_batch('pemasukkan_harian_tb',$data_pemasukkan_laundry);

      #Bagian Return   
        if($execute){
          $this->db->trans_complete();
          return_ajax(1,"Berhasil menambah data pemasukkan harian");
        }else{
          return_ajax(0,"Gagal Menambah data pemasukkan");
        }
    }

    public function get_form_master()
    {  
      #Bagian Inisialisasi
         $type = post('type');
         $id   = post('id');

      #Bagian Proses
         if($type=="update"){
            $data_checking_empty = array($id);
            checking_empty($data_checking_empty);
            $where      = array("id_master_absen"=>$id);
            $data_master  = h_crud_get_1_data("master_absen_tb",$where);

            if(empty($data_master)){
               echo "<center>Data Tidak Ditemukan</center>";
               return 0;
            }
         }else{
            $data_master  = null;
         }


      #Bagian Return
         $data['master'] = $data_master;
         $data['type'] = $type;
         load_empty_view('form_master',$data);
    }

    public function get_form_password()
    {  
      #Bagian Inisialisasi
      #Bagian Proses
      #Bagian Return
         load_empty_view('form_password');
    }
 

    public function delete_conf()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();
        $tgl = post('tanggal');
        $date  =date_create($tgl); 
        $date_ = date_format($date,"Y-m-d");
        $data_checking_empty = array($tgl);
        checking_empty($data_checking_empty);

      #Bagian Proses 
        $execute = h_crud_delete('pemasukkan_harian_tb','tanggal',$date_);
        
      #Bagian Return
        if(!$execute){
          return_ajax(0,"Gagal Menghapus Pemasukkan Harian");
        }else{
          $this->db->trans_complete();
          return_ajax(1,"Berhasil Menghapus Pemasukkan Harian");
        }
    }

    public function delete_conf_satuan()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();
        $id             = $this->input->post('id');
        $data_checking_empty = array($id);
        checking_empty($data_checking_empty);

      #Bagian Proses  
        $execute = h_crud_delete('pemasukkan_harian_tb','id',$id);
        
      #Bagian Return
        if(!$execute){
          return_ajax(0,"Gagal Menghapus Data Pemasukkan");
        }else{
          $this->db->trans_complete();
          return_ajax(1,"Berhasil Menghapus Data Pemasukkan");
        }
    }

    public function update_conf($type="jamak"){  
      #Bagian Inisialisasi  
        check_ajax_request();
        $this->db->trans_start(); 
        $data_pemasukkan_laundry = array();
        $total   = post('total');
        $harga   = post('harga');
        $jumlah   = post('jumlah');
        $tanggal = post('tgl');
        $nama    = post('name');
        $jenis_laundry = post('jenis_laundry');
        $ukuran  = post('ukuran');
        $ukuran  = post('ukuran');

      #Bagian Proses 
        if($type=="jamak"){
          h_crud_delete('pemasukkan_harian_tb','tanggal',$tanggal); 
        }

        foreach ($total as $key => $row) {
           //jenis laundry
           $jl = explode("___", $jenis_laundry[$key]);

           //ukuran
           $u = explode("___", $ukuran[$key]);

           $tmp = array(
              'tanggal'=>$tanggal,
              'nama_customer'=>$nama[$key],
              'id_jenis_laundry'=>$jl[0],
              'id_ukuran'=>$u[0],
              'harga_laundry'=>$harga[$key],
              'jumlah_laundry'=>$jumlah[$key],
              'total'=>$total[$key],
              'updated_by'=>get_session('id_user')
           );
           array_push($data_pemasukkan_laundry, $tmp);
        }
        $execute = h_crud_tambah_batch('pemasukkan_harian_tb',$data_pemasukkan_laundry);

      #Bagian Return   
        if($execute){
          $this->db->trans_complete();
          return_ajax(1,"Berhasil Update data pemasukkan harian");
        }else{
          return_ajax(0,"Gagal Update data pemasukkan");
        }
    }
    
    #Pemasukkan Lainya-----------------------------------------------------------
    public function pemasukkan_lainya()
    {
      #Bagian Inisialisasi 
      #Bagian Proses
        $data_pemasukkan = h_crud_get_data('pemasukkan_lainya_view');
        
      #Bagian Return
        $data['data']   = $data_pemasukkan; 
        load_view('list_pemasukkan_lainya',$data,'List Pemasukkan Lainya');
    }

    public function get_pemasukkan_lainya()
    {
      #Bagian Inisialisasi 
      #Bagian Proses
        $data_pemasukkan = h_crud_get_data('pemasukkan_lainya_view');
        
      #Bagian Return
        $data['data']   = $data_pemasukkan;
        load_empty_view('list_pemasukkan_lainya_table',$data);
    }

    public function get_form_pemasukkan_lainya()
    {  
      #Bagian Inisialisasi
         $type = post('type');
         $id   = post('id');

      #Bagian Proses
         if($type=="update"){
            $data_checking_empty = array($id);
            checking_empty($data_checking_empty);
            $where      = array("id"=>$id);
            $data_pemasukkan  = h_crud_get_1_data("pemasukkan_lainya_tb",$where);

            if(empty($data_pemasukkan)){
               echo "<center>Data Tidak Ditemukan</center>";
               return 0;
            }
         }else{
            $data_pemasukkan  = null;
         }


      #Bagian Return
         $data['data'] = $data_pemasukkan;
         $data['type'] = $type;
         load_empty_view('form_pemasukkan_lainya',$data);
    }

    function tambah_pemasukkan_lainya_conf(){
        #Bagian Inisialisasi
          check_ajax_request();
          $this->db->trans_start();

          $tanggal    = post('tanggal');
          $jumlah     = post('jumlah');
          $keterangan = post('keterangan');

          $data_checking_empty = array($tanggal,$jumlah,$keterangan);
          checking_empty($data_checking_empty);

        #Bagian Proses 
          $data = array(
            "tanggal"=>$tanggal,
            "jumlah"=>$jumlah,
            "keterangan"=>$keterangan,
            "updated_by"=>get_session('id_user')
          );
          $execute = h_crud_tambah('pemasukkan_lainya_tb', $data);

        #Bagian Return
          if($execute){
            $this->db->trans_complete();
            return_ajax(1,"Berhasil Menambah Pemasukkan Lainya");
          }else{
            return_ajax(0,"Gagal Menambah Pemasukkan Lainya");
          }
    }

    public function update_pemasukkan_lainya_conf(){
       #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();
        $id         = post('id');
        $tanggal    = post('tanggal');
        $jumlah     = post('jumlah');
        $keterangan = post('keterangan');

        $data_checking_empty = array($tanggal,$jumlah,$keterangan);
        checking_empty($data_checking_empty);

      #Bagian Proses 
        //eksekusi guys
        $data = array(
            "tanggal"=>$tanggal,
            "jumlah"=>$jumlah,
            "keterangan"=>$keterangan
          );
        $execute = h_crud_update('pemasukkan_lainya_tb',$data,'id',$id);
        
      #Bagian Return
        if($execute){
          $this->db->trans_complete();
          return_ajax(1,"Berhasil Mengubah Data Pemasukkan");
        }else{
          return_ajax(0,"Gagal Mengubah Data Pemasukkan");
        }
        echo json_encode($return);
    }

    public function delete_pemasukkan_lainya_conf()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();
        $id             = $this->input->post('id');
        $data_checking_empty = array($id);
        checking_empty($data_checking_empty);

      #Bagian Proses  
        $execute = h_crud_delete('pemasukkan_lainya_tb','id',$id);
        
      #Bagian Return
        if(!$execute){
          return_ajax(0,"Gagal Menghapus Data Pemasukkan");
        }else{
          $this->db->trans_complete();
          return_ajax(1,"Berhasil Menghapus Data Pemasukkan");
        }
    }
    #End Pemasukkan Lainya-------------------------------------------------------

    #Pengeluaran Lainya-----------------------------------------------------------
    public function pengeluaran()
    {
      #Bagian Inisialisasi 
      #Bagian Proses
        $data_pemasukkan = h_crud_get_data('pengeluaran_view');

      //untuk graphic max 30 data
      $data_graphic = $this->m_keuangan_laundry->get_graphic_pengeluaran();

      #Bagian Return 
        $data['data_graphic']    = $data_graphic;  
        $data['data']   = $data_pemasukkan; 
        load_view('list_pengeluaran',$data,'List Pemasukkan Lainya');
    }

    public function get_pengeluaran_lainya()
    {
      #Bagian Inisialisasi 
      #Bagian Proses
        $data_pemasukkan = h_crud_get_data('pemasukkan_lainya_view');
        
      #Bagian Return
        $data['data']   = $data_pemasukkan;
        load_empty_view('list_pemasukkan_lainya_table',$data);
    }

    public function get_form_pengeluaran()
    {  
      #Bagian Inisialisasi
         $type = post('type');
         $id   = post('id');

      #Bagian Proses
         if($type=="update"){
            $data_checking_empty = array($id);
            checking_empty($data_checking_empty);
            $where      = array("id"=>$id);
            $data_pemasukkan  = h_crud_get_1_data("pengeluaran_tb",$where);

            if(empty($data_pemasukkan)){
               echo "<center>Data Tidak Ditemukan</center>";
               return 0;
            }
         }else{
            $data_pemasukkan  = null;
         }


      #Bagian Return
         $data['data'] = $data_pemasukkan;
         $data['type'] = $type;
         load_empty_view('form_pengeluaran',$data);
    }

    function tambah_pengeluaran_conf(){
        #Bagian Inisialisasi
          check_ajax_request();
          $this->db->trans_start();

          $tanggal    = post('tanggal');
          $jumlah     = post('jumlah');
          $keterangan = post('keterangan');

          $data_checking_empty = array($tanggal,$jumlah,$keterangan);
          checking_empty($data_checking_empty);

        #Bagian Proses 
          $data = array(
            "tanggal"=>$tanggal,
            "jumlah"=>$jumlah,
            "keterangan"=>$keterangan,
            "updated_by"=>get_session('id_user')
          );
          $execute = h_crud_tambah('pengeluaran_tb', $data);

        #Bagian Return
          if($execute){
            $this->db->trans_complete();
            return_ajax(1,"Berhasil Menambah Pengeluaran");
          }else{
            return_ajax(0,"Gagal Menambah Pengeluaran");
          }
    }

    public function update_pengeluaran_conf(){
       #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();
        $id         = post('id');
        $tanggal    = post('tanggal');
        $jumlah     = post('jumlah');
        $keterangan = post('keterangan');

        $data_checking_empty = array($tanggal,$jumlah,$keterangan);
        checking_empty($data_checking_empty);

      #Bagian Proses 
        //eksekusi guys
        $data = array(
            "tanggal"=>$tanggal,
            "jumlah"=>$jumlah,
            "keterangan"=>$keterangan
          );
        $execute = h_crud_update('pengeluaran_tb',$data,'id',$id);
        
      #Bagian Return
        if($execute){
          $this->db->trans_complete();
          return_ajax(1,"Berhasil Mengubah Data Pengeluaran");
        }else{
          return_ajax(0,"Gagal Mengubah Data Pengeluaran");
        }
        echo json_encode($return);
    }

    public function delete_pengeluaran_conf()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();
        $id             = $this->input->post('id');
        $data_checking_empty = array($id);
        checking_empty($data_checking_empty);

      #Bagian Proses  
        $execute = h_crud_delete('pengeluaran_tb','id',$id);
        
      #Bagian Return
        if(!$execute){
          return_ajax(0,"Gagal Menghapus Data Pengeluaran");
        }else{
          $this->db->trans_complete();
          return_ajax(1,"Berhasil Menghapus Data Pengeluaran");
        }
    }
    #End Pengeluaran Lainya-------------------------------------------------------
}