<?php
class M_keuangan_laundry extends CI_Model {
   public function __construct()
   {
        parent::__construct();
   }

   public function get_graphic_pemasukkan_harian()
   {
        $this->db->select('*');
        $this->db->from('pemasukkan_harian_view');
        $this->db->limit(31);
        $this->db->order_by('tanggal','DESC');
        $data = $this->db->get()->result();
        krsort($data); 
        $label_graph = array();
        $data_graph = array();
        foreach ($data as $key => $row) {
          array_push($label_graph, tgl_indo($row->tanggal));
          array_push($data_graph, $row->total_pemasukkan);
        }

        $data_return = array(
          'label'=>json_encode($label_graph),
          'data'=>json_encode($data_graph)
        );

        return $data_return;
   }

   public function data_graphic_bulanan()
   {
        $this->db->select('sum(total_pemasukkan) as total, tanggal, month(tanggal) as bulan,year(tanggal) as tahun');
        $this->db->from('pemasukkan_harian_view');
        $this->db->limit(10);
        $this->db->group_by('bulan,tahun');
        $this->db->order_by('tanggal','DESC');
        $data = $this->db->get()->result();
        krsort($data); 
        
        $label_graph = array();
        $data_graph = array();
        foreach ($data as $key => $row) {
          array_push($label_graph, tahun_bulan($row->tanggal));
          array_push($data_graph, $row->total);
        }

        $data_return = array(
          'label'=>json_encode($label_graph),
          'data'=>json_encode($data_graph)
        );

        return $data_return;
   }

   public function get_graphic_pengeluaran()
   {
        $this->db->select('tanggal,sum(jumlah) as jumlah');
        $this->db->from('pengeluaran_view');
        $this->db->limit(10);
        $this->db->group_by('tanggal');
        $this->db->order_by('tanggal','DESC');
        $data = $this->db->get()->result();
        krsort($data); 

        $label_graph = array();
        $data_graph = array();
        foreach ($data as $key => $row) {
          array_push($label_graph, tgl_indo($row->tanggal));
          array_push($data_graph, $row->jumlah);
        }

        $data_return = array(
          'label'=>json_encode($label_graph),
          'data'=>json_encode($data_graph)
        ); 
        return $data_return;
   }

   public function get_graphic_penggunaan_uang_laundry()
   {
        $this->db->select('tanggal,sum(jumlah) as jumlah');
        $this->db->from('pengeluaran_family_view');
        $this->db->where('insert_by',get_session('id_user'));
        $this->db->limit(10);
        $this->db->group_by('tanggal');
        $this->db->order_by('tanggal','DESC');
        $data = $this->db->get()->result();
        krsort($data); 

        $label_graph = array();
        $data_graph = array();
        foreach ($data as $key => $row) {
          array_push($label_graph, tgl_indo($row->tanggal));
          array_push($data_graph, $row->jumlah);
        }

        $data_return = array(
          'label'=>json_encode($label_graph),
          'data'=>json_encode($data_graph)
        ); 
        return $data_return;
   }

   function total_penggunaan_dana_laundry(){
      $this->db->select('sum(jumlah) as jumlah');
      $this->db->from('pengeluaran_family_view');
      $this->db->where('insert_by',get_session('id_user'));
      $data = $this->db->get()->row();
      if(!empty($data)){
         return $data->jumlah;
      }else{
         return 0;
      } 
   }

   function get_graphic_keuangan_laundry(){
      //get date 
      $this->db->select('tanggal');
      $this->db->from('data_keuangan'); 
      $this->db->limit(30);
      $this->db->group_by('tanggal'); 
      $this->db->order_by('tanggal','DESC');
      $data = $this->db->get()->result();
      $arr = array();
      $in  = array();
      krsort($data); 
      foreach ($data as $row => $value) {
         $tmp = array('debit'=>'','kredit'=>'');
         $arr[$value->tanggal] = $tmp; 
         array_push($in, $value->tanggal);
      } 

      $this->db->select('tanggal, sum(jumlah) as jumlah, jenis');
      $this->db->from('data_keuangan');
      if(!empty($in)){
        $this->db->where_in('tanggal',$in);
      } 
      $this->db->group_by('jenis,tanggal'); 
      $this->db->order_by('tanggal','DESC');
      $data = $this->db->get()->result();
      foreach ($data as $row => $value) { 
         $arr[$value->tanggal][$value->jenis] = $value->jumlah;
      } 

 
      $label_graph = array();
      $data_graph = array();
      foreach ($arr as $key => $row) {
        $debit = 0;
        if(!empty($row['debit'])){
          $debit = $row['debit'];
        }

        $kredit = 0;
        if(!empty($row['kredit'])){
          $kredit = $row['kredit'];
        }

        $val = $debit-$kredit;

        array_push($label_graph, tgl_indo($key));
        array_push($data_graph, $val );
      }

      $data_return = array(
        'label'=>json_encode($label_graph),
        'data'=>json_encode($data_graph)
      );  
      return $data_return;

   }

    function get_total_keuangan_laundry($tabel){
      //get date 
      $this->db->select('sum(jumlah) as jumlah');
      $this->db->from('data_keuangan');   
      $this->db->where('tabel',$tabel);
      $data = $this->db->get()->row();
      if (empty($data)) {
         return 0;
      }else{
        return $data->jumlah;
      }
    }

    function get_first_last_date(){ 
      $arr = new Stdclass; 
      $this->db->select('min(tanggal) as first_date, max(tanggal) as last_date ');
      $this->db->from('data_keuangan'); 
      $data = $this->db->get()->row();
      if(empty($data)){
        $arr = new Stdclass;
        $arr->first_date = date('Y-m-d');
        $arr->last_date  = date('Y-m-d');
        return $arr;
      }else{
        return $data;
      }
    } 

    function get_graphic_detail($awal,$akhir){
        $this->db->select('tipe, tabel, sum(jumlah) as jumlah');
        $this->db->from('data_keuangan');
        $this->db->where('tanggal BETWEEN "'.$awal.'" AND "'.$akhir.'"'); 
        $this->db->group_by('tabel'); 
        $data = $this->db->get()->result();  

        $label_graph = array();
        $data_graph = array();
        $data_color_bg = array();
        $data_color_line = array();
        foreach ($data as $key => $row) {
          array_push($label_graph, $row->tipe);
          array_push($data_graph, $row->jumlah);
          if($row->tabel=='pemasukkan_harian_tb'){
            $color = '0, 155, 245';
          }elseif($row->tabel=='pemasukkan_lainya_tb'){
            $color = '37, 186, 0';
          }elseif($row->tabel=='pengeluaran_tb'){
            $color = '245, 215, 22';
          }else{
            $color = '252, 3, 107';
          }
          array_push($data_color_line, 'rgba('.$color.',1)');
          array_push($data_color_bg, 'rgba('.$color.',0.2)');
        }

        $data_return = array(
          'label'=>json_encode($label_graph),
          'data'=>json_encode($data_graph),
          'bg'=>json_encode($data_color_bg),
          'line'=>json_encode($data_color_line)
        ); 
        return $data_return;
    }

    function get_total_detail($awal,$akhir){
        $this->db->select('jenis, sum(jumlah) as jumlah');
        $this->db->from('data_keuangan');
        $this->db->where('tanggal BETWEEN "'.$awal.'" AND "'.$akhir.'"'); 
        $this->db->group_by('jenis'); 
        $data = $this->db->get()->result();   
        $data_return = array(); 
        $data_return['debit'] = 0;
        $data_return['kredit'] = 0;

        if (!empty($data))  {
          foreach ($data as $row) {
            $data_return[$row->jenis] = $row->jumlah;
          }
        }
 
        return $data_return;
    }

    function get_data_detail($awal,$akhir){
        $this->db->select('*');
        $this->db->from('data_keuangan');
        $this->db->where('tanggal BETWEEN "'.$awal.'" AND "'.$akhir.'"'); 
        $this->db->order_by('tanggal','ASC'); 
        $data = $this->db->get()->result();    
        
        return $data;
    }
   
}

?>