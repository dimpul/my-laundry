<?php
class M_user extends CI_Model {
   public function __construct()
   {
        parent::__construct();
   }

   public function get_user()
   {
        #get role without karyawan and super user
        $role = array(1,3);
        $this->db->select('*');
        $this->db->from('user_tb');
        $this->db->where_not_in('role',$role);
        return $this->db->get()->result();
   }

   public function get_role_user()
   {
        #get role without karyawan and super user
        $role = array(1,3);
        $this->db->select('*');
        $this->db->from('role_tb');
        $this->db->where_not_in('id_role',$role);
        return $this->db->get()->result();
   }

   public function get_id_finger()
   {
        #get role without karyawan and super user
        $this->db->select('max(id_finger) as max');
        $this->db->from('user_tb');
        $max = $this->db->get()->row()->max;
        return $max+1;
   }

   
}

?>