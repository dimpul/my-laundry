<?php
class M_jadwal_kerja extends CI_Model {
   public function __construct()
   {
        parent::__construct();
   }

   public function get_jadwal_kerja($id_user, $tanggal)
   {
        $this->db->select('*');
        $this->db->from('jadwal_kerja_tb');
        $this->db->where('id_user',$id_user);
        $this->db->where('tanggal like',$tanggal."%");
        return $this->db->get()->result();
   }

   public function delete_jadwal($tanggal, $id_user)
   {
        $this->db->where('tanggal', $tanggal);
        $this->db->where('id_user', $id_user);
        if($this->db->delete('jadwal_kerja_tb')){
          return 1;
        }else{
          return 0;
        }

   }
}

?>