<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_kerja extends CI_Controller {

	public function __construct()
    {
	    parent::__construct();
      $this->load->model('karyawan/m_karyawan');
      $this->load->model('user/m_user');;
      $this->load->model('m_jadwal_kerja');
    }


    public function index()
    {
      #Bagian Inisialisasi        
      #Bagian Proses
        $data_karyawan = $this->m_karyawan->get_karyawan();
        
      #Bagian Return
        $data['list']   = $data_karyawan;
        load_view('list',$data,'List Data Karyawan');
    }

    public function get_form_jadwal()
    {  
      #Bagian Inisialisasi
        check_ajax_request();
         $id   = post('id');

      #Bagian Proses
         $where = array("id_user"=>$id);
         $data_user = h_crud_get_1_data('user_tb',$where); 

      #Bagian Return
         $data['data'] = $data_user;
         load_empty_view('form_jadwal',$data);
    }

    public function get_form_manage_jadwal()
    {  
      #Bagian Inisialisasi
        check_ajax_request();
         $id   = post('id_user');
         $tanggal   = post('tanggal');
         $id_jadwal = post('id_jadwal');

         $data_checking_empty = array($id);
         checking_empty($data_checking_empty,false);

      #Bagian Proses
         if(!empty($id_jadwal)){
           $data_checking_empty = array($id_jadwal);
           checking_empty($data_checking_empty,false);

           $where = array('id'=>$id_jadwal); 
           $data_jadwal = h_crud_get_1_data('jadwal_kerja_tb',$where);
           $tanggal = $data_jadwal->tanggal; 
         }else{
           $data_checking_empty = array($tanggal);
           checking_empty($data_checking_empty,false);

           $data_jadwal = array();
         }
         //get master absen
         $data_master_absen = h_crud_get_data("master_absen_tb",array(),'jam_masuk');

      #Bagian Return
         $data['data_jadwal']  = $data_jadwal;
         $data['master_absen'] = $data_master_absen;
         $data['tanggal'] = $tanggal;
         load_empty_view('form_manage_jadwal',$data);
    } 

    public function get_form_generate()
    {  
      #Bagian Inisialisasi
         check_ajax_request();
      #Bagian Proses
         $data_master_absen = h_crud_get_data("master_absen_tb",array(),'jam_masuk');
      #Bagian Return 
         $data['master_absen'] = $data_master_absen; 
         load_empty_view('form_generate_jadwal',$data);
    } 

    public function get_form_tanggal()
    {  
      #Bagian Inisialisasi
         check_ajax_request();
      #Bagian Proses
      #Bagian Return  
         load_empty_view('form_tanggal');
    } 

    public function set($id_user,$tanggalx){
      #Bagian Inisialisasi  
        //check if null
        if(empty($id_user) || empty($tanggalx)){
          set_flashsession('<i class="fa fa-close"> </i> Data tidak ditemukan');
          redirect(base_url('Jadwal_kerja'));  
        } 

        //check user
        $where      = array("id_user"=>$id_user);
        $data_user  = h_crud_get_1_data("user_tb",$where);
        if(empty($data_user)){
          set_flashsession('<i class="fa fa-close"> </i> Data tidak ditemukan');
          redirect(base_url('Jadwal_kerja'));  
        } 

      #Bagian Proses
        //change date format yang bisa digunakan untuk query
        $date    = date_create("01-".$tanggalx);
        $tanggal = date_format($date,"Y-m"); 
        $date_next = date_create("01-".$tanggalx);
        $nextmonth = date_add($date_next, date_interval_create_from_date_string('1 month')); 
        $nextmonth = date_format($date_next, 'm-Y');

        $date_prev = date_create("01-".$tanggalx);
        $prevmonth = date_add($date_prev, date_interval_create_from_date_string('-1 month')); 
        $prevmonth = date_format($date_prev, 'm-Y');

         
      #Bagian Return 
        $data['next']         = base_url('jadwal_kerja/set/').$id_user.'/'.$nextmonth;
        $data['prev']         = base_url('jadwal_kerja/set/').$id_user.'/'.$prevmonth;
        $data['data_user']    = $data_user;
        $data['tgl']          = tahun_bulan($tanggal);
        $data['calendar_date'] = $tanggal;
        load_view('set_jadwal_kerja',$data,"Set Jadwal Kerja ".$data_user->nama_lengkap,"false");
    }

    public function get_event(){

      #Bagian Inisialisasi
        $tanggal       = post('tanggal');
        $id_user       = post('id_user');

      #Bagian Proses
        //get data jadwal kerja
        $jadwal_kerja = $this->m_jadwal_kerja->get_jadwal_kerja($id_user,$tanggal);
        
        $event = array();
        foreach ($jadwal_kerja as $row) {
          if($row->jam_masuk=="00:00:00" && $row->jam_pulang=="00:00:00"){
            $title = "OFF";
            $color = "red";
          }else{
            $title = substr($row->jam_masuk,0,5)." - ".substr($row->jam_pulang,0,5);
            $color = "blue";
          }
          
          $tmp_array = array(
            "id" => $row->id,
            "title" => $title, 
            "start" => $row->tanggal."T".$row->jam_masuk,
            "end" => $row->tanggal."T".$row->jam_pulang,
            "color" => $color
          );
          array_push($event, $tmp_array);
        }
        echo json_encode($event);
    }

     public function add_jadwal_kerja()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();

        $tanggal       = post('tanggal');
        $master_absen  = post('master_absen');
        $id_user       = post('id_user');

        $data_checking_empty = array($tanggal,$id_user,$master_absen);
        checking_empty($data_checking_empty,false);
        
      #Bagian Proses
        //hapus
        $this->m_jadwal_kerja->delete_jadwal($tanggal, $id_user);

        //get masterabsen
        $where = array('id_master_absen'=>$master_absen);
        $data_master_absen = h_crud_get_1_data('master_absen_tb',$where);

        //tambah
        $data = array(
          "id_user"=>$id_user,
          "tanggal"=>$tanggal,
          "jam_masuk"=>$data_master_absen->jam_masuk,
          "jam_pulang"=>$data_master_absen->jam_pulang
        );
        $execute = h_crud_tambah('jadwal_kerja_tb', $data);

      #Bagian Return
        if($execute){
          $this->db->trans_complete();
          return_ajax(1,"Berhasil Set Jadwal Kerja");
        }else{
          return_ajax(0,"Gagal Set Jadwal Kerja");
        }
    }

    public function generate_jadwal_kerja()
    {
      #Bagian Inisialisasi
        check_ajax_request();
        $this->db->trans_start();

        $tanggal_awal   = post('tanggal_awal');
        $tanggal_akhir  = post('tanggal_akhir');
        $master_absen   = post('master_absen');
        $id_user        = post('id_user');
        $arr_tanggal    = array();

        $data_checking_empty = array($tanggal_awal,$tanggal_akhir,$master_absen,$id_user);
        checking_empty($data_checking_empty,false);
        
      #Bagian Proses
        //get date range
        $period = new DatePeriod(
             new DateTime($tanggal_awal),
             new DateInterval('P1D'),
             new DateTime($tanggal_akhir)
        );
        foreach ($period as $key => $value) {
          $tanggal = $value->format('Y-m-d'); 
          //hapus
          $this->m_jadwal_kerja->delete_jadwal($tanggal, $id_user);

          //get masterabsen
          $where = array('id_master_absen'=>$master_absen);
          $data_master_absen = h_crud_get_1_data('master_absen_tb',$where);

          //tambah
          $data = array(
            "id_user"=>$id_user,
            "tanggal"=>$tanggal,
            "jam_masuk"=>$data_master_absen->jam_masuk,
            "jam_pulang"=>$data_master_absen->jam_pulang
          );
          $execute = h_crud_tambah('jadwal_kerja_tb', $data);
        }
        
        #Bagian Return
        $this->db->trans_complete();
        return_ajax(1,"Berhasil Set Jadwal Kerja");
          
    } 
}