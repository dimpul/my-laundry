<!-- Full Calendar Io -->
<link href='<?= base_url('assets/vendors/fullcalendar/packages/core/main.css'); ?>' rel='stylesheet' />
<link href='<?= base_url('assets/vendors/fullcalendar/packages/daygrid/main.css'); ?>' rel='stylesheet' />
<script src='<?= base_url('assets/vendors/fullcalendar/packages/core/main.js'); ?>'></script>
<script src='<?= base_url('assets/vendors/fullcalendar/packages/interaction/main.js'); ?>'></script>
<script src='<?= base_url('assets/vendors/fullcalendar/packages/daygrid/main.js'); ?>'></script>
<script src='<?= base_url('assets/vendors/fullcalendar/packages/timegrid/main.js'); ?>'></script>

<style type="text/css">
  .custom1{
    border-radius: 10px !important;
  }
   .fc-day-grid-event .fc-content {
     white-space: normal; 
  }
  .btn{
    margin-bottom: 5px;
  }
</style>



<div class="row mb-4">
    <div class="col-md-5 col-xs-12 mt-2" style="font-weight: bold;">
      <div class="card bg bg-info" style="padding-bottom: 0px; color: white; "> 
        <div class="card-body" style="padding-bottom: 0px; padding-top: 15px;  "> 

    
        <h4> <b>Nama</b> : <?= $data_user->nama_lengkap; ?> </h4>
        <h4> <b>Periode</b> : <?= $tgl; ?></h4> <br> 
      </div>
    </div>
  </div>
    <div class="col-md-7 col-xs-12 mt-2" style="font-weight: bold;">
      <div class="card " style="padding-bottom: 0px;  text-align: center; border-style: solid !important; border-color: #4d7cff !important;"> 
        <div class="card-body" style="padding-bottom: 20px; padding-top: 15px;"> 
          <button class="custom1 btn  btn-primary" id="generate"> <i class="fa fa-cog fa-spin"></i> Generate </button>
          <button class="custom1 btn  btn-success" id="pilih"> <i class="fa fa-calendar"></i> Pilih Tanggal </button>
          <a class="custom1 btn  btn-warning" href="<?= $prev; ?>"> <i class="fa fa-arrow-left"></i> Sebelumnya </a>
          <a class="custom1  btn btn-info" href="<?= $next; ?>"> <i class="fa fa-arrow-right"></i> Berikutnya </a>
        </div>
    </div>
  </div>
</div>
<div id="div-alert" class="alert alert-warning" style="display: none;"> </div>
<div class="card "> 
    <div class="card-body"> 
        <center><h2> Jadwal Kerja </h2></center>
        <div id="calendar" class="mt-4"> </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  id_user = "<?= $data_user->id_user; ?>";
  var jsonEvent = null;
  var calendarEl = document.getElementById('calendar');

  function get_form(arg){
    //get and split date and time
    var tmp = arg.split("T");
    var time = "";
    var datetime = "";
    var date = "";

    var date = tmp[0];
    console.log(date);
    
    get_append_ajax("id_user="+id_user+"&tanggal="+date+"&id_jadwal=", "<?= base_url('jadwal_kerja/get_form_manage_jadwal'); ?>", "","div-alert","div", "yes" , "<i class='fa fa-pencil'></i> Set Jadwal Kerja");
  }

  function get_jadwal(arg){ 
    var id = arg['event']['id']; //id terapi
    console.log(id);
    get_append_ajax("id_user="+id_user+"&tanggal="+"&id_jadwal="+id, "<?= base_url('jadwal_kerja/get_form_manage_jadwal'); ?>", "","div-alert","div", "yes" , "<i class='fa fa-pencil'></i> Set Jadwal Kerja");
  
  }
  generate_calendar();
  function generate_calendar(){
    $.ajax({
        type: 'POST',
        url:'<?= base_url('jadwal_kerja/get_event'); ?>',
        data: "tanggal=<?= $calendar_date; ?>&id_user=<?= $data_user->id_user; ?>",
        beforeSend: function(){

          $("#calendar").empty();
          pesan_tunggu("Memuat Kalender, Mohon Tunggu . . .","div-alert","div");
        },
        success:function(data){
          reset_pesan_tunggu("div-alert","div");
          jsonEvent = JSON.parse(data);
          build_calendar();
        }
    });
  }

  function build_calendar(){
    var calendar = new FullCalendar.Calendar(calendarEl, {
      defaultDate: "<?= $calendar_date; ?>",
      locale: 'id',
      plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
      header: {
        left:'',
        center: '',
        right:'',
        //right:'title'
      },
      month: 4,
      navLinks: false, // can click day/week names to navigate views
      selectable: true,
      selectMirror: true,
      disableDragging: true, 
      select: function(arg) {
          //tanggal yg di klik
          get_form(arg['startStr']);
      },
      eventClick: function(arg) {
        //event yg di klik
          get_jadwal(arg);
      },
      agendaEventMinHeight: 100,
      editable: false ,
      eventLimit: true, // allow "more" link when too many events
      events: jsonEvent,
      allDaySlot: false,
      eventTextColor: "white",
      displayEventTime: false,
      eventTimeFormat: {
        hour: 'numeric',
        minute: '2-digit',
        meridiem: false
      }
    });

    calendar.render();
  }
</script>

<script type="text/javascript">
  $("#generate").click(function(){
      get_append_ajax("","<?= base_url('jadwal_kerja/get_form_generate'); ?>", "","div-alert","div", "yes" , "<i class='fa fa-cog fa-spin'></i> Generate Jadwal Kerja");
  });

  $("#pilih").click(function(){
      get_append_ajax("","<?= base_url('jadwal_kerja/get_form_tanggal'); ?>", "","div-alert","div", "yes" , "<i class='fa fa-calendar '></i> Pilih Tanggal");
  });
</script>