<form id="global-form" >
  <div class="form-group ">
    <label>Tanggal Awal</label>
    <div class='input-group date' >
      <input class="form-control form-control-sm" id='datetimepicker6' name="tanggal_awal"  type="text" value="" autocomplete="off" required>
    </div>
  </div>
  <div class="form-group ">
    <label>Tanggal Akhir</label>
    <div class='input-group date' >
        <input class="form-control form-control-sm" id='datetimepicker7' name="tanggal_akhir"  type="text" value="" autocomplete="off" required>
      </div>
  </div>
  <div class="form-group" id="edit">
    <label>Master Absen</label>
    <div class="input-group">
    <select class="form-control  select2" style="width: 100%;"   name="master_absen"   required="">
      <option value=""> -- Pilih -- </option> 
      <?php  foreach ($master_absen as $row) { ?>
        <?php  if($row->id_master_absen == '1'){ ?>
          <option value="<?= $row->id_master_absen; ?>"><?= $row->nama_master_absen; ?></option>
        <?php }else{ ?>
          <option value="<?= $row->id_master_absen; ?>"><?= $row->nama_master_absen." | ".$row->jam_masuk."-".$row->jam_pulang; ?></option>
      <?php } } ?>
    </select>
    </div>
  </div>
 
  <center><button id="submit-btn" class="btn btn-info" type="submit" >  Simpan </button></center>
</form>

<script type="text/javascript">

    $(function () {
        $('#datetimepicker6').datetimepicker({
           format: 'Y-MM-D'
        });
        $('#datetimepicker7').datetimepicker({
            useCurrent: false,
           format: 'Y-MM-D'
        });
        $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });
    });

</script>
<script type="text/javascript">

    $('.select2').select2({
      theme: "bootstrap"
     });
  </script>

<script type="text/javascript">
  $('#GlobalModal').modal({backdrop: 'static', keyboard: false});
   $('#global-form').submit(function(event) { 
        event.preventDefault(); 
        var values = $(this).serialize();
        simple_ajax_calendar(values+"&id_user="+id_user,"<?= base_url('jadwal_kerja/generate_jadwal_kerja'); ?>","#","Berhasil!","Gagal!","submit-btn","button");
        return false; //stop
    });
</script>