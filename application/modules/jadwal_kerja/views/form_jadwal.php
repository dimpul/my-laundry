<?php
	#inisialisasi
  $id_user    = @$data->id_user;
  $nama_user  = @$data->nama_lengkap;
?>
<form id="global-form" >
  <div class="form-group cent-hidden">
    <label>ID User</label>
    <input class="form-control" name="id_user" id="id_user" type="text" value="<?= $id_user; ?>" required>
  </div>
  <div class="form-group ">
    <label>Nama Karyawan</label>
    <input class="form-control" readonly="" style="background-color: #fff0c7;" type="text" value="<?= $nama_user; ?>" required>
  </div>
  <div class="form-group ">
    <label>Tanggal</label>
    <div class="input-group">
    <input class="form-control tgl" id="tgl" name="tanggal" type="text" >
    </div>
  </div>
 
  <center><button id="submit-btn" class="btn btn-info" type="submit" > <i class="fa fa-calendar"></i>  Lihat Jadwal </button></center>
</form>

<script type="text/javascript">
    $('.tgl').datetimepicker({
          format: 'MM-Y',
          useCurrent: true,
          widgetPositioning: {
            horizontal: 'left',
            vertical: 'bottom'
        }
      });
  </script>

<script type="text/javascript">
   $('#global-form').submit(function(event) { 
        event.preventDefault(); 
        var id_user = $("#id_user").val();
        var tgl = $("#tgl").val();
        window.location = "<?= base_url('jadwal_kerja/set/'); ?>"+id_user+"/"+tgl;
        return false;
    });
</script>