<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
            <div id="div-alert" class="alert alert-warning" style="display: none;"> </div>
            <div class="row" style="padding-bottom: 10px;">
                <div class="col-md-7 cent-left"><h3 class="">Jadwal Kerja Karyawan</h3></div>
                <div class="col-md-5 cent-right" id="btn_config">
                    <button class="btn btn-info cent-hidden btn-sm clickable_row_button" id="tambah"  data-toggle="tooltip" data-placement="bottom" title="Jadwal Kerja"><i class="fa fa-calendar"></i> Jadwal Kerja</button>
                </div>
            </div>
              
              <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table class="table table-hover datatable">
                      <thead>
                        <tr>
                            <th>ID User</th>
                            <th>ID Finger</th>
                            <th>Nama Lengkap</th>
                            <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($list as $row) { ?>
                            <tr class="clickable_row" data-id="<?= $row->id_user; ?>" class="menu">
                                <td><?= $row->id_user; ?></td>
                                <td><?= $row->id_finger; ?></td>
                                <td><?= $row->nama_lengkap; ?></td>
                                <td><?= active($row->status_akun); ?></td>
                            </tr>
                        <?php }  ?> 
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
    </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $("#tambah").click(function(){
        get_append_ajax("id="+tr_id, "<?= base_url('jadwal_kerja/get_form_jadwal'); ?>", "","div-alert","div", "yes" , "<i class='fa fa-calendar'></i> Jadwal Kerja Karyawan");
    });
  });
</script>