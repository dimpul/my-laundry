<form id="global-form" >
  <div class="form-group ">
    <label>Tanggal</label>
    <input class="form-control" name="tanggal" readonly="" style="background-color: #fff0c7;" type="text" value="<?= $tanggal; ?>" required>
  </div>
  <?php
    if(!empty($data_jadwal)){ ?>
      <div class="form-group " id="jk">
        <label>Jadwal Kerja</label>
        <input class="form-control" readonly="" style="background-color: #d1fdff;" type="text" value="<?= $data_jadwal->jam_masuk." - ".$data_jadwal->jam_pulang; ?>" readonly>
      </div>
  <?php } ?>
  <div class="form-group" id="edit">
    <label>Master Absen</label>
    <div class="input-group">
    <select class="form-control select2" name="master_absen" style="width: 100%;"  required="">
      <option value=""> -- Pilih -- </option>
      <?php  foreach ($master_absen as $row) { ?>
           <?php  if($row->id_master_absen == '1'){ ?>
          <option value="<?= $row->id_master_absen; ?>"><?= $row->nama_master_absen; ?></option>
        <?php }else{ ?>
          <option value="<?= $row->id_master_absen; ?>"><?= $row->nama_master_absen." | ".$row->jam_masuk."-".$row->jam_pulang; ?></option>
      <?php } } ?>
    </select>
    </div>
  </div>
 
  <center><button id="submit-btn" class="btn btn-info" type="submit" >  Simpan </button></center>
</form>


<?php if(!empty($data_jadwal)){ ?>
  <script type="text/javascript">
      $('#edit').addClass('cent-hidden');
      $('#submit-btn').removeClass('btn-info');
      $('#submit-btn').attr('type','button');
      $('#submit-btn').empty();
      $('#submit-btn').append('Update');

      $('#submit-btn').click(function(){
         var text  = $('#submit-btn').text();
         if(text=="Update"){
            $('#edit').removeClass('cent-hidden');
            $('#jk').addClass('cent-hidden');
            $('#submit-btn').addClass('btn-info');
            $('#submit-btn').attr('type','submit');
            $('#submit-btn').empty();
            $('#submit-btn').append('Simpan');
         }
      })
  </script>
<?php } ?>

<script type="text/javascript">

    $('.select2').select2({
      theme: "bootstrap"
     });
  </script>

<script type="text/javascript">
   $('#global-form').submit(function(event) { 
        event.preventDefault(); 
        var values = $(this).serialize();
        simple_ajax_calendar(values+"&id_user="+id_user,"<?= base_url('jadwal_kerja/add_jadwal_kerja'); ?>","#","Berhasil!","Gagal!","submit-btn","button");
        return false; //stop
    });
</script>