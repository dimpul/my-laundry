<style type="text/css">
    .card-header{
        background-image: linear-gradient(to right, white , #4d7cff);
    }

    .borderx{
        border-radius: 10px !important; 
        border-style: solid !important; 
        border-color: #4d7cff !important;  
        border-top: 0px !important;
        border-top-right-radius: 0px !important;
    }
</style>


    <div class="card" id="card-upload">
        <div class="card-body">
            <h3><i class="fa fa-upload"></i> Upload Data Log Finger</h3>
            <br>
            <div class="form-group" >
              <label>Pilih File Excel</label><br>
              <input type="file" class="form-control" id="fileUploader" name="fileUploader" accept=".xls, .xlsx"/>
            </div> 

            <div id="alert" class="cent-hidden" >
                <div class="alert alert-info"> <i class="fa fa-check"></i> Data dapat diupload </div>
                <div class="cent-right">
                <button class="btn btn-info " id="selanjutnya"> <i class="fa fa-arrow-right"></i> Selanjutnya </button>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="card cent-hidden" id="card-progress">
        <div class="card-body">
            <h3><i class="fa fa-spin fa-spinner"></i> <div id="lblprgs" style="display: inline-block;">0</div>%
            Menyimpan Log Finger</h3>
            <br>
            <div class="progress" style="height: 40px;">
              <div id="prgs" class="progress-bar progress-bar-striped bg-success progress-bar-animated" role="progressbar"  style="width: 5%"></div>
            </div>
            <div class="cent-hidden" id="ulang" style="padding-top: 10px;"><center>
                <button class="btn btn-success" onclick="location.reload();"> <i class="fa fa-plus"></i> Upload Lagi </button>
                <!-- <button class="btn btn-success" onclick="generate();"> <i class="fa fa-arrow-right"></i> Langkah Selanjutnya  </button> -->
            </center></div>
        </div>
    </div>


  <br>
    <div class="card cent-hidden" id="resultx">
        <div class="card-body">
            <h3><i class="fa fa-check"></i> Upload Result</h3>
            <br>
            <div id="accordion" id="accordion">

              
        </div>
    </div>


</br></br>
    
    
     <script>
        var data_finger = [];
        var data_fix = [];
        var jumlah_row = 0;
        var jumlah_submit = 0;
        var date_before = null;
        var date_after = null;
        var year = null;

        function fixing_array(array_text){
            var mentah = JSON.parse(array_text);
            //console.log(mentah);
            var x = 0;
            var id = null;
            var jumlah_hari = 0;
            var tmp;
            $.each(mentah, function( index, value ) {
             
             if(x==0){
                //ambil header nya
                $.each(value, function( key, val ) {
                    var arr = val.split("  ");
                    var tmp_arr = arr[0].split(":");
                    id  = tmp_arr[1];

                    //get date
                    tmp_arr      = key.split("  ");
                    arr          = tmp_arr[0].split(":");
                    var arr_date = arr[1].split('-');
                    year         = arr[0];
                    date_before  = year+"/"+arr_date[0];
                    date_after   = year+"/"+arr_date[1];
                });
                x++;
             }else if(x==1){
                /*$.each(value, function( key, val ) {
                    data_finger[id][val] = null;
                    jumlah_hari++;
                });*/
                x++;
             }else if(x==2){
                y = 1;
                data_finger[0] = id;
                $.each(value, function( key, val ) {
                    data_finger[y] = val;
                    y++;
                }); 
                data_fix.push(data_finger);
                data_finger = [];
                x=0;
             }
            });
            console.log(data_fix);

            $("#alert").removeClass('cent-hidden');
        }

        $(document).ready(function(){
              $("#fileUploader").change(function(evt){
                    $("#alert").addClass('cent-hidden');
                    var selectedFile = evt.target.files[0];
                    var reader = new FileReader();
                    reader.onload = function(event) {
                      var data = event.target.result;
                      var workbook = XLSX.read(data, {
                          type: 'binary'
                      });
                      workbook.SheetNames.forEach(function(sheetName) {
                        
                          var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                          var json_object = JSON.stringify(XL_row_object);
                          //document.getElementById("jsonObject").innerHTML = json_object;
                          
                          fixing_array(json_object);
                        })
                    };

                    reader.onerror = function(event) {
                      console.error("File could not be read! Code " + event.target.error.code);
                    };

                    reader.readAsBinaryString(selectedFile);
              });
        });

        function checking(i){
            if(jumlah_row==i){ 
                    $("#resultx").removeClass('cent-hidden');
                    $("#ulang").removeClass('cent-hidden'); 
            }
        }

        $("#selanjutnya").click( function(event){
            console.log('save clicked');
            $("#card-progress").removeClass('cent-hidden');
            $("#card-upload").addClass('cent-hidden');
            jumlah_row = data_fix.length;         
            var i = 1;   
            $.each(data_fix,async function( key, value ) {
                /*var xyz = JSON.stringify(value);
                console.log(key);*/
                const result = await fromServer(value);
                console.log(result);
                checking(i);
                i++;
            });
        });


        function fromServer(value){

          return new Promise((resolve, reject) => {
             $.ajax({
                type: 'POST',
                url:'<?= base_url('log_finger_new/upload_log_conf'); ?>',
                data: "date="+date_before+"&data="+value,
                success:function(data){
                    jumlah_submit++;
                    var persen = (jumlah_submit/jumlah_row)*100;
                    $("#prgs").css("width",persen+"%");
                    $("#lblprgs").empty();
                    $("#lblprgs").append(persen);
                    console.log(jumlah_row+"/"+jumlah_submit+" = "+persen);
                    resolve(value);
                    $("#accordion").append(data);
                }
            });
          });
        } 

        /*var totalQuotes = 0;
        async function domManipulation(){
            var counter = 0;
            while (counter < 6) {
                var data = await fromServer();
                console.log(data);
                counter++;
            }

            $.each(data_fix,async function( key, value ) {
                const result = await fromServer(value);
            });
        }
*/
        

        // reject runs the second function in .then
        

    </script>
