<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_finger_new extends CI_Controller {

	public function __construct()
    {
	    parent::__construct();
      check_session();
      $this->load->model('m_log_finger');
      $this->load->helper('koreksi_absen');
    }

    /*public function index()
    {
       echo "xyz";
    }*/

    public function upload()
    {  
      #Bagian Inisialisasi
      #Bagian Proses
      #Bagian Return
    	load_view('upload_log');
    }

    public function upload_log_conf(){
      #Bagian Inisialisasi
      check_ajax_request();
      $this->db->trans_start();
      $data = post('data');
      $tgl  = post('date');
      $arrx  = array();

      #Bagian Proses
        //ambil tahun & bulan nya aja
        $date     = str_replace('/', '-', $tgl);
        $date_fix = date('Y-m', strtotime($date));
        $date_indo = date('M-Y', strtotime($date));

      $arr = explode(",", $data);
      $id_finger = null;
      foreach ($arr as $key => $row) {
        if($key==0){
          $id_finger = $row;
          $data_user = $this->m_log_finger->get_karyawan_by_finger($id_finger);
          if(empty($data)){
            $text = "<div class='alert alert-danger'> <i class='fa fa-close'> </i> Tidak ada Karyawan dengan ID Finger: $id_finger </div>";
            echo $this->generate_return($text);
            return 0;
          }
          $data_user->tgl_absen = $date_indo;
          $text = "<table class='table table-hover'>";
          $text .= "<tr><th> Tanggal </th><th> Jam Absen</th><th> Keterangan</th></tr>";
        }else{
          
          $arr_day = explode(" ", $row);
          foreach ($arr_day as $row) {
              if(empty($row)){
                continue;
              }
              $text .= "<tr>";
              $text .= "<td>$date_fix"."-"."$key</td>";
              $text .= "<td>$row</td>";
              $text .= "<td>";
              //search dulu biar gak redudant
              $data = array(
                'id_finger'=>$id_finger,
                'tanggal'=>$date_fix."-".$key,
                'jam'=>$row
              );
              $check = h_crud_get_1_data("log_finger_tb",$data);
              if(empty($check)){
                $execute = h_crud_tambah('log_finger_tb', $data);
                if($execute){
                  #generate absen
                  $generate_absen = h_koreksi_absen_generate($data_user->id_user,$id_finger,$date_fix."-".$key);
                  if($generate_absen){
                    $text2 = "& berhasil generate";
                  }else{
                    $text2 = "& Gagal generate";
                  }

                  $text .= "<i class='fa fa-check'> </i> Berhasil Ditambahkan ".$text2;
                }else{
                  $text .= "<i class='fa fa-close'> </i> Gagal Ditambahkan";
                }
              }else{
                $text .= "<i class='fa fa-close'> </i> Data Duplikat";
                continue;
              }
          }
          $text .= "</td>";
          $text .= "</tr>";
        }
        
      }
      $text .= "</table>";

      #generate sisa apakah sakit/izin/alfa
      h_koreksi_absen_generate_sisa($data_user->id_user,$id_finger,$date_fix);

      #Bagian Return
      $this->db->trans_complete(); 
      echo $this->generate_return($text,$data_user);
      
    }

    public function generate_return($text,$data_user){
      $return = '<div class="card" style="margin-bottom: 10px;">
        <div class="card-header card-header" id="headingOne">
          <h5 class="mb-0">
            <button class="btn btn-info" data-toggle="collapse" data-target="#card'.$data_user->id_user.'" aria-expanded="true" aria-controls="collapseOne">
               '.$data_user->tgl_absen." | ".$data_user->nama_lengkap.'
            </button>
          </h5>
        </div>
        <div class="borderx collapse" id="card'.$data_user->id_user.'" class="" aria-labelledby="headingOne" data-parent="#accordion">
          <div class="card-body">
            '.$text.'
          </div>
        </div>
      </div>';
      return $return;
    }

    public function contoh_transaction(){
  		$this->db->trans_start();
  		$query1 = $this->db->query('AN SQL QUERY...');
  		$query2 = $this->db->query('ANOTHER QUERY...');
  		$this->db->trans_complete();

  		/*
  			jika query 2 gagal maka query 1 akan dibatalkan
  		*/
    }

}