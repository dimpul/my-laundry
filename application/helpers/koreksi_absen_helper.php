<?php 
  
function h_koreksi_absen_generate($id_user,$id_finger,$tanggal){
      #Bagian Inisialisasi 
		  $sys = get_instance();
      check_ajax_request(); 
        
      #Bagian Proses
          $where = array('id_user'=>$id_user,'tanggal'=>$tanggal);

          //hapus log absen hari itu
            h_crud_delete_ver2("log_absen_tb",$where); 

          /*//get jadwal kerja
           $keterangan = null;
           $jadwal_kerja = h_crud_get_1_data("jadwal_kerja_tb",$where);
           if(!empty($jadwal_kerja)){
           	  if($jadwal_kerja->jam_masuk=='00:00:00' && $jadwal_kerja->jam_pulang=='00:00:00'){
	              $keterangan = 'OFF';
	           	  $log_masuk  = '00:00:00';
	              $log_pulang = '00:00:00';
           	  } 
           } */

      //get log finger , first and last
      $sys->db->select('min(jam) as absen_datang, max(jam) as absen_pulang');
			$sys->db->from('log_finger_tb');
			$sys->db->where('id_finger',$id_finger);
			$sys->db->where('tanggal',$tanggal);
			$log_finger = $sys->db->get()->row();  
			
		   if(empty($log_finger) /* && $keterangan !== 'OFF'*/ ){
           	  $keterangan = 'Alfa';
           	  $log_masuk  = '00:00:00';
              $log_pulang = '00:00:00'; 
		   }else{
		   	      $log_masuk  = $log_finger->absen_datang;
              $log_pulang = $log_finger->absen_pulang;
           	  $keterangan = 'Masuk';
		   } 
   
           $data = array(
            "id_user"=>$id_user,
            "tanggal"=>$tanggal,
            "in"=>$log_masuk,
            "out"=>$log_pulang, 
            "total_terlambat"=>'00:00:00',
            "total_pulang_cepat"=>'00:00:00', 
            "keterangan"=>$keterangan
           );

           $execute = h_crud_tambah('log_absen_tb', $data); 
           return $execute;    
}

function h_koreksi_absen_generate_sisa($id_user,$id_finger,$month_year){ 
    $begin = new DateTime( $month_year.'-01' );
    $end   = new DateTime( $begin->format('Y-m-t') ); 
    $end   = $end->modify( '+1 day' );  

    $interval  = new DateInterval('P1D');
    $daterange = new DatePeriod($begin, $interval ,$end);

    foreach($daterange as $date){
        $tgl = $date->format('Y-m-d');

        #cek absen (bukan log) jika ada abaikan 
        $sys = get_instance();
        $sys->db->select('*');
        $sys->db->from('log_absen_tb');
        $sys->db->where('id_user',$id_user);
        $sys->db->where('tanggal',$tgl);
        $data = $sys->db->get()->row();
        if(!empty($data)){
          continue;
        }

        //set absen jika gak masuk
        $where = array('id_user'=>$id_user,'tanggal'=>$tgl);
        $jadwal_kerja = h_crud_get_1_data("jadwal_kerja_tb",$where);
         if(!empty($jadwal_kerja)){
            if($jadwal_kerja->jam_masuk=='00:00:00' && $jadwal_kerja->jam_pulang=='00:00:00'){
              $keterangan = 'OFF';
            } 
         }else{
            $keterangan = 'Alfa';
         }

         $log_masuk  = '00:00:00';
         $log_pulang = '00:00:00';
         $data = array(
            "id_user"=>$id_user,
            "tanggal"=>$tgl,
            "in"=>$log_masuk,
            "out"=>$log_pulang, 
            "total_terlambat"=>'00:00:00',
            "total_pulang_cepat"=>'00:00:00', 
            "keterangan"=>$keterangan
         );

           $execute = h_crud_tambah('log_absen_tb', $data);
    }
}

?>