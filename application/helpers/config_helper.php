<?php
function estes($param){
	#fungsi ini untuk memotong proses dan show param
	ob_get_clean();
    echo "<pre>";
    print_r($param);
    echo "</pre>";
    exit;
}

function post($name){
	$dimpul = get_instance();
	return $dimpul->input->post($name);
}

function post_all(){
	$dimpul = get_instance();
	return $dimpul->input->post();
}

function get($name){
	$dimpul = get_instance();
	return $dimpul->input->get($name);
}

function get_all(){
	$dimpul = get_instance();
	return $dimpul->input->get();
}

function estes_query(){ 
	$dimpul = get_instance();
	$query = $dimpul->db->last_query();
	estes($query);
}

function check_ajax_request(){
	$dimpul = get_instance();
	if (!$dimpul->input->is_ajax_request()) {
	   exit('Bad Request');
	}
}

function check_selected($d1, $d2){
	if($d1==$d2){
		return "selected";
	}
}

function check_checked($d1, $d2){
	if($d1==$d2){
		return "checked";
	}
}

function check_session(){
	$dimpul = get_instance();
	if(empty($dimpul->session->userdata('id_user'))){
		redirect(base_url());
	}
}
function get_session($sess){
	$dimpul = get_instance();
	return $dimpul->session->userdata($sess);
}

function estes_session(){ 
	$dimpul = get_instance();
	estes($dimpul->session->userdata());
}

function return_ajax($status,$text){
  $return = array('stt'=>$status,'message'=>$text);
  echo json_encode($return);
  exit;
}

function checking_empty($data,$return_ajax=true){
  foreach ($data as $row) {
      if(empty($row)){
      	if($return_ajax==true){
        	return_ajax(0,"Maaf, Data Tidak Lengkap");
      	}else{
      		echo '<div class="alert alert-danger"><i class="fa fa-close"> </i> Maaf, Data Tidak Lengkap</div>';
      		exit;
      	}
      }
  }
}

function set_flashsession($text){
	$dimpul = get_instance();
	$dimpul->session->set_flashdata('flash', $text);
}

function get_flashsession(){
	$dimpul = get_instance();
	return $dimpul->session->flashdata('flash');
}

function granted_access($is_menu="true"){
	$dimpul = get_instance();
	$uri_all = $dimpul->uri->uri_string;
	$uri1 = $dimpul->uri->segment(1); 
	$uri2 = $dimpul->uri->segment(2); 
	if(!empty($uri2)){
		$uri_x = $uri1."/".$uri2;
	}else{
		$uri_x = $uri1;
	}  

	//get role
	$dimpul->db->select("A.role_id");
	$dimpul->db->from("menu_akses_tb A");
	$dimpul->db->join("menu_tb B","A.id_menu = B.id_menu");

	if($is_menu=="true"){
		$dimpul->db->where("B.uri",$uri_all);
	}else{
		$dimpul->db->where("(B.uri='".$uri_x."' OR B.uri='".$uri1."')");	
	}
	
	$dimpul->db->where("A.role_id",$dimpul->session->userdata('role'));
	$data = $dimpul->db->get()->result();  
	$role = array();
	if(empty($data)){
		set_flashsession('<i class="fa fa-close"> </i> Akses Ditolak');
		redirect(base_url('home'));
	}else{
		foreach ($data as $row) {
			array_push($role, $row->role_id);
		}
	}
	$sess_role = get_session('role');
	if(!in_array($sess_role, $role)){
		set_flashsession('<i class="fa fa-close"> </i> Akses Ditolak');
		redirect(base_url('home'));
	}
}

function active($stat){
	if($stat==1){
		return "<div class='badge badge-success'> Aktif </div>";
	}else{
		return "<div class='badge badge-danger'> Tidak Aktif </div>";
	}
}

function bulan($i){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);

	return $bulan[$i];
}

function tahun_bulan($tanggal){
	$arr = explode('-', $tanggal);
	$tahun = $arr[0];
	$bulan = bulan((int)$arr[1]);
	return $bulan." ".$tahun;
}

function time_to_second($time){ 
  $str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $time);

  sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds); 
  $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;

  return $time_seconds;
}

function second_to_time($second){
  return gmdate("H:i:s", $second);
}

function tgl_indo($tanggal){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal); 
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

function rupiah($angka,$mata_uang=null){
	if($angka==0){
		return "-";
	}
	$hasil_rupiah = $mata_uang. number_format($angka,0,',',',');
	return $hasil_rupiah;
 
}

?>