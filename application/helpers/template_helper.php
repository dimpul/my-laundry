<?php

function load_view($view,$data=null,$title='Sinar Clean Laundry',$is_menu="true"){
	//design halaman setelah login
	$dimpul = get_instance();
    check_session();
    granted_access($is_menu);
	ob_start();
	$header['title'] = "SCL - ".$title;
	$header['menu'] = get_menu(get_session('role'));
	$dimpul->load->view('template/header',$header);
    $dimpul->load->view($view,$data);
    $dimpul->load->view('template/footer');
}

function load_view_login($view,$data=null,$title='Sinar Clean Laundry'){
	//design halaman setelah login
	$dimpul = get_instance();
	ob_start();
	$header['title'] = "SCL - ".$title;
	$header['menu'] = get_menu(1);
	$dimpul->load->view('template/header_login',$header);
    $dimpul->load->view($view,$data);
    $dimpul->load->view('template/footer_login');
}


function load_empty_view($view,$data=null){
    check_session();
	$dimpul = get_instance();
	ob_start();
    $dimpul->load->view($view,$data);
}

function get_menu($role){
	$dimpul = get_instance(); 

	//get menu
	$dimpul->db->select('A.*');
	$dimpul->db->from('menu_tb A');
	$dimpul->db->join('menu_akses_tb B','A.id_menu = B.id_menu');
	$dimpul->db->where('B.role_id',$role);
	$dimpul->db->order_by('(CASE WHEN A.id_menu=99 THEN 9999 END),A.id_menu,  A.nama_menu','ASC');
	$data = $dimpul->db->get()->result();

	$menu = array();
	foreach ($data as $key => $row) {
		$parent = $row->parent;
		if($parent == 0){
			$id_menu = $row->id_menu;
			$menu[$id_menu] = $row;
			$menu[$id_menu]->child = [];
			unset($data[$key]);
		}
	}

	foreach ($data as $key => $row) {
		$parent = $row->parent;
		if(empty($menu[$parent])){
			continue;
		} 
		array_push($menu[$parent]->child, $row);
	}  
	return $menu;
}


?>