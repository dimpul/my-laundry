</div>
</div>
<footer class="footer">
  <div class="w-100 clearfix">
    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Develop by Centree</span>
    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">V 1.0</span>
  </div>
</footer>
</div>

<!-- MODAL GLOBAL -->
<div class="modal fade" id="GlobalModal" role="dialog">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="GlobalModalTitle">Title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> 
      </div>
      <div class="modal-body" id="GlobalModalBody"></div>
    </div>
  </div> 
</div>
<!-- Modal Ends -->

  <script src="<?= base_url('assets/'); ?>js/off-canvas.js"></script>
  <!-- <script src="<?= base_url('assets/'); ?>js/hoverable-collapse.js"></script> -->
  <script src="<?= base_url('assets/'); ?>js/template.js"></script>
  <script src="<?= base_url('assets/'); ?>js/settings.js"></script>
  <script src="<?= base_url('assets/'); ?>js/todolist.js"></script> 
  <script src="<?= base_url('assets/'); ?>js/dashboard.js"></script>
  <script src="<?= base_url('assets/'); ?>js/todolist.js"></script>
  <script src="<?= base_url('assets/'); ?>js/data-table.js"></script>
  <script src="<?= base_url('assets/'); ?>vendors/select2/dist/js/select2.full.min.js"></script>
  <script lang="javascript" src="<?= base_url('assets/'); ?>vendors/convertExcelToJSON/js/xlsx.full.min.js"></script>

  <script src="<?= base_url('assets/vendors/datetimepicker/build/js/moment.js'); ?>"></script>
  <script src="<?= base_url('assets/vendors/datetimepicker/build/js/bootstrap-datetimepicker.min.js'); ?>"></script>
  
  <script type="text/javascript">
    
  //datetimepicker 
  $('.datepicker').datetimepicker({
      format: 'DD-MM-YYYY',
      useCurrent: true,
      widgetPositioning: {
        horizontal: 'left',
        vertical: 'bottom'
    }
  }); 
  $('.CentreeTgl').CentreeTgl();
  $('.centree-datetimepicker').datetimepicker({
      format: 'DD/MM/YYYY',
      useCurrent: false,
      sideBySide: true
  }).on('dp.change', function (e) { 
  //get attribut
      var name    = $(this).attr('name');
      var name_tmp  = name.split("___");
      var name_real = name_tmp[1];
      var value     = $(this).val();

      //change format date
      var arr = value.split("/");
      var datex = arr[2] + "-" + arr[1] + "-" + arr[0];

      //change date
      $("input[name='"+name_real+"']").val(datex); 

  }); 

  $('.select2').select2({
    theme: "bootstrap"
   });
  </script
  </script>
  
</body>


</html>