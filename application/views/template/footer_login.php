

  <!-- plugins:js -->
  <script src="<?= base_url('assets/'); ?>vendors/js/vendor.bundle.base.js"></script>
  <script src="<?= base_url('assets/'); ?>vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  
  <script src="<?= base_url('assets/'); ?>js/dashboard.js"></script>
  <script src="<?= base_url('assets/'); ?>js/todolist.js"></script>
  <script src="<?= base_url('assets/'); ?>js/dimpul.js"></script>
  <!-- End custom js for this page-->
</body>


</html>