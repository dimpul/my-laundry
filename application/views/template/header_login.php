<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?= $title; ?></title>
  <link rel="stylesheet" href="<?= base_url('assets/'); ?>vendors/fa/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?= base_url('assets/'); ?>css/horizontal-layout/style.css">
  <link rel="shortcut icon" href="<?= base_url('assets/'); ?>images/ico.png" />
  <link rel="stylesheet" href="<?= base_url('assets/css/cent.css'); ?>" />
  <link rel="stylesheet" href="<?= base_url(); ?>assets/vendors/sweetalert2/dist/sweetalert2.min.css">

  <script src="<?= base_url(); ?>assets/vendors/sweetalert2/dist/sweetalert2.min.js"></script> 
  <script src="<?= base_url('assets/'); ?>vendors/js/vendor.bundle.base.js"></script>
  <script src="<?= base_url('assets/'); ?>vendors/js/vendor.bundle.addons.js"></script> 
</head>

<body>
  <div class="container-scroller">
    <div class="main-panel" style="padding-top: 0px;">